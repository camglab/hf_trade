# FROM python:3.7-slim

# RUN apt-get update
# RUN apt-get install -y python3-dev build-essential

# ADD ./requirements.txt .
# RUN pip install -r requirements.txt --no-cache-dir

# VOLUME [ "/trader" ]

# ENV PYTHONPATH=/hf_trade:/trader
FROM registry.gitlab.com/camglab/hf_trade:cache
ADD . /hf_trade

WORKDIR /trader

ENV HDF5_USE_FILE_LOCKING "FALSE"

CMD python /hf_trade/scripts/run_simulation.py