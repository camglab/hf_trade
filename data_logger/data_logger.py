import os
import re
import json
from datetime import datetime, timedelta, timezone
import tables as tb
import requests
import logging
import traceback



H5_FORMAT_COMPILER = re.compile("(.*?)_([0-9]{8}).h5")


class TableRestfulAPI(object):

    def __init__(self, host: str):
        self.host = host
        self.table_api = "/hf_data/createSimDataTable"
        self.data_api = "/hf_data/sim_data"
        self.get_tables_api = "/hf_data/getSimDataTables"
        self.drop_table_api = "/hf_data/dropSimDataTable/{table_name}"
        
    def create_table(self, table_name: str):
        payload = {"table": table_name}
        resp = requests.post(self.host+self.table_api, json=payload)
        status = resp.status_code
        if status == 200:
            logging.info(f"[CREATE TABLE] [{status}] {resp.text}")
            return True
        else:
            logging.error(f"[CREATE TABLE ERROR] [{status}] {resp.text}")
            return False
        
    def get_tables(self):
        resp = requests.get(self.host+self.get_tables_api)
        if resp.status_code == 200:
            return resp.json()["data"]
        else:
            raise IOError(f"{resp.status_code} {resp.text}")
    
    def drop_table(self, table_name: str):
        resp = requests.post(self.host+self.drop_table_api.format(table_name=table_name))
        if resp.status_code == 200:
            logging.info(f"[DROP TABLE] [{resp.status_code}] {resp.text}")
            return True
        else:
            logging.error(f"[DROP TABLE ERROR] [{resp.status_code}] {resp.text}")
            return False
            
    def post_data(self, table_name: str, strategy: str, data: list):
        payload = {
            "table": table_name,
            "strategy": strategy,
            "data": [dict(ts=doc.pop("timestamp"), tag=doc.pop("tag", ""), data=json.dumps(doc)) for doc in data]
        }
        resp = requests.post(self.host+self.data_api, json=payload)
        status = resp.status_code
        if status == 200:
            logging.info(f"[POST DATA] [{status}] {resp.text}")
            return True
        else:
            logging.error(f"[POST DATA ERROR] [{status}] {resp.text}")
            return False
    

class DataCatcher(object):

    def __init__(self, log_file: str, data_dir: str, tzinfo: timezone = timezone.utc, ignore_heads: set=None):
        self.log_file = log_file
        self.data_dir = data_dir
        self.tzinfo = tzinfo
        self.sync_cache = {}
        if ignore_heads:
            self.ignore_heads = ignore_heads
        else:
            self.ignore_heads = set()

        if os.path.exists(log_file):
            with open(log_file) as f:
                self.sync_cache.update(json.load(f))
        
    def init_cache(self):
        updated = False
        dirs = os.listdir(self.data_dir)
        for path in sorted(dirs):
            matched = H5_FORMAT_COMPILER.match(path)
            if matched:
                fname, date = matched.groups()
                if fname in self.ignore_heads:
                    continue
                if fname not in self.sync_cache:
                    filepath = os.path.join(self.data_dir, path)
                    with tb.File(filepath, "r") as tfile:
                        t_tables = {}
                        for node in tfile.list_nodes("/"):
                            if isinstance(node, tb.Table):
                                t_tables[node.name] = 0

                    self.sync_cache[fname] = {
                        "date": date,
                        "tables": t_tables
                    }
                    updated = True
        if updated:
            self.dump_cache()
        return updated

    def dump_cache(self):
        with open(self.log_file, "w") as f:
            json.dump(self.sync_cache, f)

    def update_cache(self, fname: str, date: str, tname: str, pos: int):
        self.sync_cache[fname]["tables"][tname] = pos
        self.sync_cache[fname]["date"] = date

    def remove_cache(self, fname: str, tname: str):
        if fname in self.sync_cache:
            cache = self.sync_cache[fname]
            if tname in cache["tables"]:
                del cache["tables"][tname]
            if not len(cache["tables"]):
                del self.sync_cache[fname]
        

    def files(self):
        return list(self.sync_cache.keys())
    
    def tables(self):
        return {fname: list(info["tables"].keys()) for fname, info in self.sync_cache.items()}

    def iter_by_date(self, fname: str):
        today = datetime.now(self.tzinfo)
        cache = self.sync_cache[fname]
        log_date = datetime.strptime(cache["date"], "%Y%m%d").replace(tzinfo=self.tzinfo)
        while today >= log_date:
            date_str = log_date.strftime("%Y%m%d")
            filepath = os.path.join(self.data_dir, f"{fname}_{date_str}.h5")
            if os.path.exists(filepath):
                if date_str != cache["date"]:
                    for key in list(cache["tables"].keys()):
                           ["tables"][key] = 0
                with tb.File(filepath, "r") as tfile:
                    for tname, pos in list(cache["tables"].items()):
                        ttable = tfile.get_node("/", tname)
                        data = ttable[pos:]
                        keys = list(data.dtype.names)
                        result = [dict(zip(keys, item)) for item in data]
                        for name, value in ttable.description._v_types.items():
                            if value == "string":
                                for doc in result:
                                    doc[name] = doc[name].decode()
                        yield (date_str, tname, pos, result)
            log_date += timedelta(days=1)

    def reset_table(self, fname: str, date: str):
        cache = self.sync_cache[fname]
        filepath = os.path.join(self.data_dir, f"{fname}_{date}.h5")
        if not os.path.exists(filepath):
            return 
        cache["date"] = date
        with tb.File(filepath, "r") as tfile:
            t_tables = {}
            for node in tfile.list_nodes("/"):
                if isinstance(node, tb.Table):
                    t_tables[node.name] = 0

            cache["tables"] = t_tables

    def iter_date(self, fname: str):
        today = datetime.now(self.tzinfo)
        cache = self.sync_cache[fname]
        log_date = datetime.strptime(cache["date"], "%Y%m%d").replace(tzinfo=self.tzinfo)
        while today >= log_date:
            date_str = log_date.strftime("%Y%m%d")
            yield date_str
            log_date += timedelta(days=1)
 

    def iter_tables(self, fname: str, date: str):
        cache = self.sync_cache[fname]
        filepath = os.path.join(self.data_dir, f"{fname}_{date}.h5")
        if os.path.exists(filepath):
            with tb.File(filepath, "r") as tfile:
                for tname, pos in list(cache["tables"].items()):
                    ttable = tfile.get_node("/", tname)
                    data = ttable[pos:]
                    keys = list(data.dtype.names)
                    result = [dict(zip(keys, item)) for item in data]
                    for name, value in ttable.description._v_types.items():
                        if value == "string":
                            for doc in result:
                                doc[name] = doc[name].decode()
                        elif value.startswith("int"):
                            for doc in result:
                                doc[name] = int(doc[name])
                    yield (tname, pos, result)


    
class DataLogger(object):

    def __init__(self, api: TableRestfulAPI, data_root: str, cache_dir: str, batch_size: int = 1000, ignore_heads="orders"):
        self.data_root = data_root
        self.cache_dir = cache_dir
        self.exist_tables_file = os.path.join(self.cache_dir, "exist_tables.json")
        for path in (data_root, cache_dir):
            if not os.path.exists(path):
                os.makedirs(path, exist_ok=True)
        self.exist_tables = set()
        if ignore_heads:
            self.ignore_heads = set(ignore_heads.split(","))
        else:
            self.ignore_heads = set()
        if os.path.exists(self.exist_tables_file):
            with open(self.exist_tables_file, "r") as f:
                tbs = json.load(f)
            self.exist_tables.update(tbs)
        self.api = api
        self.batch_size = batch_size
    
    def dump_exist_tables(self):
        with open(self.exist_tables_file, "w") as f:
            json.dump(list(self.exist_tables), f)
    
    def scan(self):
        for dir_path in os.listdir(self.data_root):
            data_path = os.path.join(self.data_root, dir_path)
            if os.path.isdir(data_path):
                dc = DataCatcher(
                    log_file=os.path.join(self.cache_dir, f"{dir_path}_cache.json"),
                    data_dir=data_path,
                    ignore_heads=ignore_heads
                )
                dc.init_cache()

    def update(self):
        for dir_path in os.listdir(self.data_root):
            data_root = os.path.join(self.data_root, dir_path)
            if os.path.isdir(data_root):
                dc = DataCatcher(
                    log_file=os.path.join(self.cache_dir, f"{dir_path}_cache.json"),
                    data_dir=data_root,
                    ignore_heads=ignore_heads
                )
                for fname in dc.files():
                    for date in dc.iter_date(fname):
                        if date != dc.sync_cache[fname]["date"]:
                            dc.reset_table(fname, date)
                        for tname, pos, data in dc.iter_tables(fname, date):
                            if not len(data):
                                continue
                            table_name = f"{fname}_{tname}"
                            if self.ensure_table(table_name):
                                for shift in range(0, len(data), self.batch_size):
                                    records = data[shift:shift+self.batch_size]
                                    logging.info(f"[post data] [{table_name} {dir_path} {date} {pos}] {len(records)}")
                                    result = self.post_data(table_name, dir_path, records)
                                    if result:
                                        pos += len(records)
                                        dc.update_cache(fname, date, tname, pos)
                                        dc.dump_cache()

    def post_data(self, table_name: str, dir_path: str, records: list, retry: int=3):
        while retry:
            try:
                return self.api.post_data(table_name, dir_path, records)
            except:
                stack = traceback.format_exc()
                logging.error(f"[post data] [{table_name} {dir_path}] \n{stack}")


            retry -= 1
            logging.warning(f"[post data]  [{table_name} {dir_path}] left {retry} times")

        raise TimeoutError(f"Failed post data for {table_name} {dir_path}")

    def ensure_table(self, table_name: str):
        if table_name in self.exist_tables:
            return True
        result = self.api.create_table(table_name)
        if result:
            self.exist_tables.add(table_name)
            self.dump_exist_tables()
            return True
        else:
            return False
            
    
if __name__ == "__main__":
    import os
    import sys
    import logging

    logging.basicConfig(level=logging.INFO)

    data_root = os.environ.get("DATA_ROOT", "/data")
    cache_dir = os.environ.get("CACHE_DIR", "/cache_dir")
    host = os.environ.get("SERVER_HOST", "http://172.31.44.238:8888")
    batch_size = int(os.environ.get("BATCH_SIZE", "1000"))
    ignore_heads = os.environ.get("IGNORE_HEADS", "orders")
    api = TableRestfulAPI(host)
    dl = DataLogger(
        api,
        data_root,
        cache_dir,
        batch_size,
        ignore_heads
    )

    commands = sys.argv[1:]
    if not commands:
        commands = ["update"]
    for command in commands:
        if command == "scan":
            dl.scan()
        elif command == "update":
            dl.update()
    
