#! /bin/bash

python data_logger.py scan

scaned=true

while true
do
    minute=`date +%M`
    if (($minute == 00))
    then 
        if ! $scaned
        then
            echo `date` scan
            python data_logger.py scan
            
            scaned=true
        fi
    else
        scaned=false
    fi
    echo `date` update
    python data_logger.py update
    sleep $SLEEP_TIME
done
