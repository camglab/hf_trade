import hf_trade.local_backtest as backtest
from datetime import datetime
from itertools import chain
import pandas as pd
import numpy as np
import copy
import heapq
import collections
import time
# from float import float
from hf_trade.local_backtest import generatePartial, updatePartialDiff, UTC
from pathlib import Path
# from Strategy.DepthDriver import DepthDriver
from decimal import Decimal
import gc
from hf_trade.engine import OrderMatchType, OrderType, OrderStatus
import logging
import json



# 本策略仅用于研究limit_order的成交


class LimitDealResearchStrategy(backtest.HFTemplate):

    ntype = Decimal

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.trade_symbol = 'btc_usd_cq'
        self.mem_ts = None
        self.server_ts = None
        # self.signal_agent = DepthDriver(online=False, csv_recorder_signal=False)

        self.maker_order_gap = 500  # 发单间隔
        self.maker_order_mem_ts = None

        self.base_line = 0
        self.cancel_ts = 1

        self.pending_order_mem = dict()

        self.final_research_info = []
        self.symbol = ""
        self.start_time = 0
        self.end_time = 0

    def onInit(self):
        self.base_line = self.ntype("0.0002")
        # path = f'{self.data_save_root}/{self.exchange}/{self.symbol}/line{float(self.base_line)}_cts{self.cancel_ts}_{self.exchange}_{self.symbol}_{self.date}.csv'
        # if Path(path).exists():
        #     raise ValueError(
        #         f"{self.symbol}/line{float(self.base_line)}_cts{self.cancel_ts}_{self.exchange}_{self.symbol}_{self.date}.csv ~")
        print('init finish')
        self.start_time = datetime.now().timestamp()
        print("start at", datetime.fromtimestamp(self.start_time))

    def onL2Trade(self, trade):
        pass
        # self.signal_agent.feed_trade_flow(trade)

    def onL2Update(self, depth):

        cur_depth = self.getOrderbook(instrument_id=self.symbol)
        self.server_ts = cur_depth['timestamp']
        # cur_ts = int(self.server_ts / 80)
        #
        # if self.mem_ts is None:
        #     self.mem_ts = cur_ts
        # else:
        #     if self.mem_ts != cur_ts:
        #         self.signal_agent.flush_orderbook(cur_depth)
        #         self.mem_ts = cur_ts

        mo_ts = int(self.server_ts / self.maker_order_gap)
        if self.maker_order_mem_ts is None:
            self.maker_order_mem_ts = mo_ts
        else:
            if self.maker_order_mem_ts != mo_ts:
                self.make_order_logic()
                self.maker_order_mem_ts = mo_ts
                self.sendMessage("ldr_strategy:message", json.dumps({"message": "make_order"}))
                self.output("data/test", {"timestamp": self.current_time(), "pending": len(self.pending_order_mem)})
                # self.stop_and_raise("Test stop.")
        self.check_mem_and_cancel()

    def check_mem_and_cancel(self):
        cur_depth = self.getOrderbook(instrument_id=self.symbol)
        cur_ts = cur_depth['timestamp']
        for o_id in self.pending_order_mem.keys():
            o_case = self.pending_order_mem[o_id]
            if o_case['is_cancel'] is False:
                if cur_ts - o_case['server_ts'] > self.cancel_ts * 1000:
                    self.cancelOrder(order_id=o_id)
                    o_case['is_cancel'] = True

    def send_order_boost(self, instrument_id, order_type, side, price, qty, server_ts, match_type=OrderMatchType.Limit):
        """
        :param instrument_id:   交易品种  btc_usdt_swap
        :param order_type: backtest.OrderType.Close / Open  开仓平仓
        :param side:   1/-1  多/空
        :param price:  3.1415926 价格
        :param qty: 2.71 数量
        :param server_ts:  1515151  发单的 server_ts
        :param match_type:   OrderMatchType.Limit / PostOnly  撮合类型
        :return: 订单id
        """
        info_dict = dict()
        resp = self.sendOrder(instrument_id=instrument_id, order_type=order_type, side=side, price=price, qty=qty,
                              match_type=match_type)
        info_dict['order_id'] = resp['order_id']
        info_dict['server_ts'] = server_ts
        info_dict['side'] = side
        info_dict['order_status'] = 0
        info_dict['filled_type'] = 0
        info_dict['filled_ts'] = -1
        info_dict['is_cancel'] = False
        info_dict['price'] = price
        return info_dict

    def handel_after_send(self, resp):
        self.pending_order_mem[resp['order_id']] = resp

    def make_order_logic(self):
        cur_depth = self.getOrderbook(instrument_id=self.symbol)
        server_ts = cur_depth['timestamp']

        mp = (cur_depth['asks'].keys()[0] + cur_depth['bids'].keys()[0]) / self.ntype('2')

        sell_price = mp * (self.ntype('1') + self.base_line)
        buy_price = mp * (self.ntype('1') - self.base_line)

        buy_resp = self.send_order_boost(instrument_id=self.symbol, order_type=OrderType.Open, side=1, price=buy_price,
                                        qty=10, server_ts=server_ts)
        self.handel_after_send(buy_resp)
        sell_resp = self.send_order_boost(instrument_id=self.symbol, order_type=OrderType.Open, side=-1,
                                        price=sell_price, qty=10, server_ts=server_ts)
        self.handel_after_send(sell_resp)

    def handle_order_after_finish(self, order_dict, order_status, filled_type='0'):
        order_dict['order_status'] = order_status
        order_dict['filled_type'] = filled_type
        self.final_research_info.append(
            [order_dict['server_ts'], order_dict['side'], order_dict['order_status'], order_dict['filled_type'],
             order_dict['price'], self.server_ts]
        )

    def onOrder(self, order):
        order_status = order['status']
        order_id = order['order_id']

        if order_id in self.pending_order_mem.keys():
            if order_status == OrderStatus.Filled:
                order_case = self.pending_order_mem.pop(order_id)
                trade_type = ",".join(
                    map(lambda item: f"{item['transaction_id']}:{item['trade_type']}", order['trades'].values()))
                self.handle_order_after_finish(order_dict=order_case, order_status=1, filled_type=trade_type)
            elif order_status == OrderStatus.Canceled:
                order_case = self.pending_order_mem.pop(order_id)
                self.handle_order_after_finish(order_dict=order_case, order_status=-1)

    def onL2Partial(self, depth):
        # logging.info(f"[partial] {depth}")
        self.onL2Update(depth)
        

    def onStop(self):
        self.end_time = datetime.now().timestamp()
        print("stop at", datetime.fromtimestamp(self.end_time))
        print("duration", self.end_time - self.start_time)
        pass
        # if len(self.final_research_info):
        #     key_dict = ['server_ts', 'side', 'order_status', 'filled_type', 'price', 'filled_ts']
        #     res = pd.DataFrame(self.final_research_info, columns=key_dict)
        #     path = f'{self.data_save_root}/{self.exchange}/'
        #     Path(path).mkdir(exist_ok=True)
        #     path = path + self.symbol + '/'
        #     Path(path).mkdir(exist_ok=True)
        #     res.to_csv(
        #         f'{path}line{float(self.base_line)}_cts{self.cancel_ts}_{self.exchange}_{self.symbol}_{self.date}.csv',
        #         index=False)
        #     del res
        #     gc.collect()


def run_backtest(instrument_id: str, begin: int, end: int):
    backtest.ADJUST_TYPE = "decimal"
    exchange = instrument_id.split(".")[0]
    lbt = backtest.Backtest.with_exchange(exchange, cache_dir="./cache")
    # lbt.data_pretreatment("2021-03-04", [instrument_id])
    # return
    lbt.enable_data_pretreatment = False
    lbt.engine.time_tag = "timestamp"
    lbt.init_strategy(
        LimitDealResearchStrategy,
        {"symbol": instrument_id,
         "cancel_ts": 2}
    )
    start = datetime.now()
    lbt.run(
        [instrument_id],
        begin, 
        end
    )
    stop = datetime.now()
    print("FINISHED", start, stop, stop-start)
    orders, transactions = lbt.engine.make_trade_log()
    print("Average pending time(ms):", (orders["update_time"] - orders["create_time"]).mean())
    # print(transactions)


def run_simulation():  
    import sys
    from hf_trade.simulation import RealTimeSimulation
    """
    python examples/LimitDealResearchStrategy.py REDIS_URL SYMBOL

    """

    url = sys.argv[1]
    symbols = list(sys.argv[2:])

    # 初始化模拟
    rts = RealTimeSimulation.from_params(
        url, 
        order_log_dir="./ldr_orders"
    )
    # 初始化策略
    rts.init_strategy(
        LimitDealResearchStrategy,
        {"symbol": symbols[0], "ntype": Decimal}
    )
    # 输入品种启动
    rts.start(symbols)


def run_simulation_with_config():
    from hf_trade.simulation import RealTimeSimulation
    RealTimeSimulation.from_config(
        "trader_config.json",
        LimitDealResearchStrategy
    )



if __name__ == "__main__":

    logging.basicConfig(
        level=logging.INFO, 
        format="%(asctime)s | %(levelname)s | %(module)s:%(lineno)d | %(message)s",
    )

    run_backtest(
        "binance.eth_usdt_swap",
        int(datetime(2021, 3, 4, 0, 0, tzinfo=UTC).timestamp()*1000),
        int(datetime(2021, 3, 4, 1, tzinfo=UTC).timestamp()*1000),
    )
    # run_simulation()
    # run_simulation_with_config()