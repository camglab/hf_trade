from sortedcontainers import SortedList, SortedDict
from operator import neg
from itertools import chain, groupby
import pandas as pd
import numpy as np
from datetime import datetime, timedelta, timezone
from enum import Enum
import tables
import simplejson as json
import os
import copy
from pathlib import Path
from hf_trade.data_source import DataSource
from typing import List, Union
from functools import partial


UTC = timezone(timedelta())


class SendMessageType(Enum):
    Order = 1
    Cancel = 2


class RecieveMessageType(Enum):
    OrderStatusChanged = 1
    Trade = 2


class OrderType(Enum):
    Open = 1
    Close = 2


class OrderMatchType(Enum):
    Limit = 'limit'
    PostOnly = 'post_only'


class OrderStatus(Enum):
    Error = -1
    New = 1
    Pending = 2
    Filled = 3
    Canceled = 4
    End = 5


FINISHED_STATUS = {OrderStatus.Filled, OrderStatus.Canceled, OrderStatus.Error}


def makeSortedDepth(sd, depths):
    for price, volume in depths:
        sd[price] = volume


# 根据orderbook分组生成全量快照
def generatePartial(orderbook):
    _ob = orderbook.copy()
    # 这里用SortedDict是因为深度数据的需要同时满足按值(价格)存取和按顺序(挡位)存取
    asks = SortedDict()
    bids = SortedDict(neg)
    makeSortedDepth(asks, orderbook["asks"])
    makeSortedDepth(bids, orderbook["bids"])
    _ob["asks"] = asks
    _ob["bids"] = bids
    return _ob


# 更新快照
def updatePartial(orderbook, updates):
    orderbook["timestamp"] = updates["timestamp"]
    orderbook["localtime"] = updates["localtime"]
    for name in ["asks", "bids"]:
        dlist = updates[name]
        sd = orderbook[name]
        for i in range(len(dlist)):
            if dlist[i][1]:

                sd[dlist[i][0]] = dlist[i][1]
            # 如果value为0，则删除深度
            else:
                if dlist[i][0] in sd:
                    del sd[dlist[i][0]]


# 快照替换为增量
def updatePartialDiff(orderbook, updates):
    orderbook["timestamp"] = updates["timestamp"]
    orderbook["localtime"] = updates["localtime"]
    for name in ["asks", "bids"]:
        dlist = updates[name]
        sd = orderbook[name]
        for i in range(len(dlist)):
            # 当前orderbook包含该深度
            if dlist[i][0] in sd:
                # 做差得增量
                # _a = dlist[i][1] - sd[dlist[i][0]]
                # 深度更新有量，则更新当前档口
                if dlist[i][1]:
                    sd[dlist[i][0]], dlist[i][1] = dlist[i][1], dlist[i][1] - sd[dlist[i][0]]
                # 深度更新无量，删除档口
                else:
                    dlist[i][1] = -sd[dlist[i][0]]
                    del sd[dlist[i][0]]
                # 深度更新量改为曾量
            # 当前orderbook不包含该深度(当前深度为0)， 全量即增量不用做差。
            else:
                # 深度更新有量，则更新当前档口
                if dlist[i][1]:
                    sd[dlist[i][0]] = dlist[i][1]


def find_partial_time(node):
    p = node.read_where("action == 1", field="localtime")
    return np.array(sorted(set(p)))


def make_partial_log(tfile, group):
    array = find_partial_time(group["depth"])
    node = tfile.create_array(group, "ptime", array)
    node.flush()
    return node


def ensure_ptimearray(tfile, group):
    if "depth" in group and ("ptime" not in group):
        print(f"make partial timelist {group._v_name}")
        make_partial_log(tfile, group)


def log_origin_partial_depth(filename: str, instrument_ids: list):
    tfile = tables.File(filename, "a")
    rgroup = tfile.get_node("/")
    for iid in instrument_ids:
        ensure_ptimearray(tfile, rgroup[iid])
    tfile.close()


def generate_ptimearray(tfile):
    for group in tfile.list_nodes("/"):
        ensure_ptimearray(tfile, group)


def get_partial_time(group, begin_time):
    _a = group["ptime"][:]
    pos = _a.searchsorted(begin_time, "right") - 1
    if pos < 0:
        pos = 0
    return _a[pos]


def read_snapshot(filename, instrument_ids, duration_ms=5000):
    data = {}
    for iid in instrument_ids:
        _d = pd.read_hdf(filename, f"/{iid}/merged")
        _d["begintime"] = list(map(lambda dt: int(dt.timestamp() * 1000), _d.index.to_pydatetime()))
        _d["endtime"] = _d["begintime"] + duration_ms
        data[iid] = _d

    return data


def slice_snapshot(frames: dict, begin: int, end: int):
    results = []
    for iid, data in frames.items():
        begin_pos, end_pos = data["begintime"].values.searchsorted(begin), data["endtime"].values.searchsorted(end)
        if data["begintime"].iloc[begin_pos] > begin:
            if begin_pos > 0:
                begin_pos -= 1
        if data["endtime"].iloc[end_pos] == end:
            end_pos += 1

        frame = data.iloc[begin_pos:end_pos]
        frame["instrument_id"] = iid
        docs = [(doc["endtime"], 0, doc) for doc in frame.to_dict("record")]
        results.append(docs)
    return results


# 聚合迭代增量数据
# 合并增量数据不需要逐个计算，在存储的时候同一组数据是是连在一起的，通过计算得到的边界值批处理可以大幅加速。
def iterOrderBookSequence(instrument_id: str, depth: pd.DataFrame):
    depth["index"] = depth.index
    # 按id分组得到每组的action和localtime,timestamp
    joined = depth.groupby("id")[["action", "localtime", "timestamp"]].first()
    # 按id和msg_type分组得到每组的ask和bid的起止地址
    table = depth.groupby(["id", "msg_type"])["index"].agg(**{"begin": "first", "count": "count"})
    table["end"] = table["begin"] + table["count"]
    data = table.reset_index().pivot("id", "msg_type", ["begin", "end"]).sort_index(1, [1, 0]).fillna(0)
    data["action"] = joined.action
    data["localtime"] = joined.localtime
    data["timestamp"] = joined["timestamp"]
    # data = data.reset_index()
    # 将两表合并后可以得到每次增量的timestamp localtime action ask起止地址 bid起止地址

    # 这里用nparray截取速度更快
    dvalues = depth.values
    if len(data.columns) != 7:
        data = pd.DataFrame(
            data,
            columns=pd.MultiIndex(
                [['begin', 'end', 'action', 'localtime', 'timestamp'], [1, 2, '']],
                [[0, 1, 0, 1, 2, 3, 4], [0, 0, 1, 1, 2, 2, 2]],
                names=[None, 'msg_type']
            )
        ).fillna(0)

    for bb, be, ab, ae, action, localtime, timestamp in data.values:
        ltime = int(localtime)
        _d = {
            "localtime": ltime,
            "timestamp": int(timestamp),
            "asks": dvalues[int(ab):int(ae), 3:5],
            "bids": dvalues[int(bb):int(be), 3:5],
            "instrument_id": instrument_id
        }
        # yield 返回分组增量。
        yield (ltime, int(action), _d)


side_map = {
    1: 1,
    2: -1,
}


# 处理并返回trade数据        
def iterTradeSequence(instrument_id: str, frame: pd.DataFrame):
    # 这里没有迭代pandas而是迭代np数组所以速度快很多
    for row in frame.values:
        localtime = int(row[5])
        _t = {
            "trade_id": str(row[1]),
            "price": row[2],
            "qty": row[3],
            "side": side_map[int(row[4])],
            "localtime": localtime,
            "timestamp": int(row[6]),
            "instrument_id": instrument_id
        }
        yield (localtime, 3, _t)


# trade和depth增量按时间聚合排序
def genSequence(instrument_id: str, trade: pd.DataFrame, orderbook: pd.DataFrame):
    return sorted(chain(iterOrderBookSequence(instrument_id, orderbook), iterTradeSequence(instrument_id, trade)),
                  key=lambda tp: tp[0])


def slicedSequence(instrument_id, table, begin, end, group_size=3600000):
    pf = get_price_factor(symbol=instrument_id)
    qf = get_qty_factor(instrument_id)
    timelist = list(range(begin, end, group_size))
    if timelist[-1] < end:
        timelist.append(end)
    dtime = table.col("localtime")
    for i in range(len(timelist) - 1):
        bpos = dtime.searchsorted(timelist[i])
        epos = dtime.searchsorted(timelist[i + 1])
        data = pd.DataFrame(table[bpos:epos])
        adjust_data(data, ADJUST_TYPE, pf, qf)

        yield from iterOrderBookSequence(instrument_id, data)


def makeJoinedSequence(filename, instrument_ids, begin: int, end: int, group_size=3600000):
    tfile = tables.File(filename, "r")
    timelist = list(range(begin, end, group_size))
    if timelist[-1] < end:
        timelist.append(end)
    sequence = [{"range": (timelist[i], timelist[i + 1]), "data": {}} for i in range(len(timelist) - 1)]
    info = {}

    for instrument_id in instrument_ids:
        dname = f"/{instrument_id}/depth"
        dnode = tfile.get_node(dname)
        dtime = dnode.col("localtime")
        tname = f"/{instrument_id}/trade"
        tnode = tfile.get_node(tname)
        ttime = tnode.col("localtime")
        info[instrument_id] = {
            "dnode": dnode,
            "tnode": tnode,
        }

        dpos = [dtime.searchsorted(t) for t in timelist]
        tpos = [ttime.searchsorted(t) for t in timelist]
        for i in range(len(timelist) - 1):
            sequence[i]["data"][instrument_id] = {
                "dpos": (dpos[i], dpos[i + 1]),
                "tpos": (tpos[i], tpos[i + 1]),
            }
    for group in sequence:
        l = []
        for instrument_id, poses in group["data"].items():
            pf = get_price_factor(symbol=instrument_id)
            qf = get_qty_factor(symbol=instrument_id)
            dpos = poses["dpos"]
            if dpos[0] < dpos[1]:
                depths = pd.DataFrame(info[instrument_id]["dnode"][dpos[0]:dpos[1]])
                adjust_data(depths, ADJUST_TYPE, pf, qf)
                
                l.append(iterOrderBookSequence(instrument_id, depths))

            tpos = poses["tpos"]
            if tpos[0] < tpos[1]:
                trades = pd.DataFrame(info[instrument_id]["tnode"][tpos[0]:tpos[1]])
                adjust_data(trades, ADJUST_TYPE, pf, qf)
                l.append(iterTradeSequence(instrument_id, trades)) 
        if l:
            yield group["range"], l
    tfile.close()


from decimal import Decimal


ADJUST_TYPE = "decimal"


PFACTOR = Decimal(1e4)


def adjustPrice(price, factor=PFACTOR):
    return Decimal(price) / factor


PFACTOR2 = {
    'btc': Decimal(1e3),
    'yfi': Decimal(1e3),
    'yfii': Decimal(1e3),
    'eth': Decimal(1e4),
    'trx': Decimal(1e10)
}


PRICE_FACTOR = {
    'btc': Decimal(1e3),
    'yfi': Decimal(1e3),
    'yfii': Decimal(1e3),
    'eth': Decimal(1e4),
    'trx': Decimal(1e10)
}


QTY_FACTOR = {}


def get_price_factor(symbol):
    base, *_ = symbol.split('_')
    return PRICE_FACTOR.get(base, Decimal(1e5))


def get_qty_factor(symbol):
    base, *_ = symbol.split('_')
    return QTY_FACTOR.get(base, Decimal(1))


def adjustQty(qty):
    return Decimal(str(qty))


def adjust_data(data: pd.DataFrame, adjust_type: str, price_factor: Decimal = Decimal(1), qty_factor: Decimal = Decimal(1)):
    if adjust_type == "decimal":
        data["price"] = data["price"].apply(partial(adjustPrice, factor=price_factor))
        data["qty"] = data["qty"].apply(adjustQty)
    elif adjust_type == "float":
        data["price"] = data["price"] / float(price_factor)
    elif adjust_type == "int":
        pf = float(price_factor)
        qf = float(qty_factor)
        data["price"] = data["price"].apply(lambda x: int(x/pf))
        data["qty"] = data["qty"].apply(lambda x: int(x*qf))


class HFTemplate(object):

    def __init__(self):
        self.instrument_ids = []
        self.c2s_queue = None
        self._oid = 0
        self._opid = 0
        self._delay_ms = 0
        self._current_time = 0
        self._orderbooks = {}

    def init(self, c2s_queue, delay_ms, params: dict):
        self.c2s_queue = c2s_queue
        self._delay_ms = delay_ms
        for key, value in params.items():
            setattr(self, key, value)

    def onInit(self):
        pass

    def onStop(self):
        pass

    def update_orderbook(self, instrument_id, orderbook):
        self._orderbooks[instrument_id] = orderbook

    def current_time(self):
        return self._current_time

    def next_order_id(self):
        self._oid += 1
        return str(self._oid)

    def next_operate_id(self):
        self._opid += 1
        return self._opid

    def _send_order(self, order):
        self.c2s_queue.add(
            (self.current_time() + self._delay_ms, self.next_operate_id(), (SendMessageType.Order, order)))

    def _make_order(self, instrument_id, order_type, side, price, qty):
        oid = self.next_order_id()
        order = {
            "order_id": oid,
            "price": price,
            "side": side,
            "instrument_id": instrument_id,
            "order_type": order_type,
            "qty": qty,
            "filled_qty": 0,
            "trades": {},
            "create_time": self.current_time(),
            "update_time": 0
        }

        self._send_order(order)
        return order

    def onPosition(self, position):
        pass

    def onOrder(self, order):
        pass

    def sendOrder(self, instrument_id, order_type, side, price, qty, match_type=OrderMatchType.Limit):
        oid = self.next_order_id()
        order = {
            "order_id": oid,
            "price": price,
            "side": side,
            "instrument_id": instrument_id,
            "order_type": order_type,
            "qty": qty,
            "filled_qty": 0,
            "trades": {},
            "create_time": self.current_time(),
            "update_time": 0,
            "status": OrderStatus.New,
            'match_type': match_type
        }
        self._send_order(order)
        return order

    def cancelOrder(self, order_id):
        self.c2s_queue.add(
            (self.current_time() + self._delay_ms, self.next_operate_id(), (SendMessageType.Cancel, order_id)))

    def update_ts(self, timestamp):
        self._current_time = timestamp

    def onL2Partial(self, depth):
        pass

    def onL2Update(self, depth):
        pass

    def onL2Trade(self, trade):
        pass

    def onSnapshot(self, snapshot):
        pass

    def onCustomizedData(self, data):
        pass

    def getOrderbook(self, instrument_id):
        return self._orderbooks[instrument_id]


class OrderQueue(object):

    def __init__(self, order, pos=0):
        self._orders = {
            order["order_id"]: {
                "order": order,
                "next": None,
                "pre": None,
                "shift": 0
            }
        }
        self._front = order["order_id"]
        self._rear = order["order_id"]
        self.front_pos = pos
        self.rear_pos = pos + order["qty"]

    def add_order(self, order, pos=0):
        self._orders[self._rear]["next"] = order["order_id"]
        orderpos = max(pos, self.rear_pos)
        pack = {
            "order": order,
            "next": None,
            "pre": self._rear,
            "shift": orderpos - self.rear_pos
        }
        self._orders[order["order_id"]] = pack
        self._rear = order["order_id"]
        self.rear_pos = orderpos + order["qty"]

    def on_depth_update(self, qty):
        pack = self._orders[self._front]
        pos = self.front_pos
        if pos >= qty:
            self.front_pos = qty
            return

        while (pos < qty):
            pos += pack["order"]["qty"]
            if pack["next"]:
                pack = self._orders[pack["next"]]
                pos += pack["shift"]
            else:
                return

        pack["shift"] = max(pack["shift"] - pos + qty, 0)

    def _on_trade(self, qty):
        if self.front_pos:
            if qty <= self.front_pos:
                self.front_pos -= qty
                return
            else:
                qty = qty - self.front_pos
                self.front_pos = 0
        order = self.get_head()
        unfilled = order["qty"] - order["filled_qty"]

        # 可以完全成交
        if qty >= unfilled:
            qty = qty - unfilled
            order["filled_qty"] += unfilled
            order["status"] = OrderStatus.Filled
            self.pop_head()
            yield order, unfilled
            if qty and self.front_pos:
                yield from self._on_trade(qty)
        # 不能完全成交
        else:
            order["filled_qty"] += unfilled - qty
            yield order, unfilled - qty

    def on_trade(self, qty):
        return list(self._on_trade(qty))

    def get_head(self):
        return self._orders[self._front]["order"]

    def pop_head(self):
        order_id = self._front
        pack = self._orders.pop(order_id)
        self._front = pack["next"]
        if self._front:
            front = self._orders[self._front]
            self.front_pos += front["shift"]
            front["shift"] = 0
            front["pre"] = None
        else:
            self._rear = None
            self.rear_pos = 0
        return pack["order"]

    def remove_order(self, order_id):
        pack = self._orders.pop(order_id)
        next_id = pack["next"]
        pre_id = pack["pre"]
        if next_id:
            self._orders[next_id]["pre"] = pre_id
            self._orders[next_id]["shift"] += pack["shift"]
        else:
            self._rear = pre_id

        if pre_id:
            self._orders[pre_id]["next"] = next_id
        else:
            self._front = next_id

        return pack["order"]

    def is_empty(self):
        return not bool(self._orders)

    def show(self):
        print(f"front = {self.front_pos}\nrear  = {self.rear_pos}\ncount = {len(self._orders)}")
        if not self._front:
            print("() []")
            return
        pack = self._orders[self._front]
        print(f"({self.front_pos})", end=" ")
        print(f"[{pack['order']['qty']} {pack['order']['filled_qty']}]")
        while (pack["next"]):
            pack = self._orders[pack["next"]]
            print(f"({pack['shift']})", end=" ")
            print(f"[{pack['order']['qty']} {pack['order']['filled_qty']}]")


class PriorityOrderQueue(OrderQueue):

    def __init__(self, order, pos=0):
        super().__init__(
            order,
            max(pos - order["qty"], 0)
        )

    def on_depth_update(self, qty):
        pack = self._orders[self._front]
        super().on_depth_update(
            max(qty - pack["order"]["qty"], 0)
        )


class Backtest(object):
    SIDE_MAP = {
        1: "bids",
        -1: "asks"
    }

    def __init__(self):
        self._orderbooks = {}
        self._orders = {}
        self._levelqueue = {}
        self._finished_order = []
        self._handlers = {
            0: self.handle_snapshots,
            1: self.handle_partial,
            2: self.handle_update,
            3: self.handle_trade,
            4: self.handle_customized
        }
        self.strategy_class = None
        self.strategy = None
        self._oid = 0
        self._tid = 0

        self._current_time = 0
        self.delay_ms = 0
        self.data_root = "."
        self.use_snapshot = False
        self.snapshot_root = ""

        self.cache_dir = "."
        if not os.path.isdir(self.cache_dir):
            os.makedirs(self.cache_dir)

        self.c2s_queue = SortedList(key=lambda item: item[:2])
        self.s2c_queue = SortedList(key=lambda item: item[:2])

        self.c2s_handlers = {
            SendMessageType.Order: self.on_send_order,
            SendMessageType.Cancel: self.on_cancel_order,
        }

        self.s2c_handlers = {
            RecieveMessageType.OrderStatusChanged: self.call_strategy_order
        }

        self._positions = {}

        self.order_queue_class = OrderQueue

        self.data_file_head = "okex_quotation"
        self.snapshot_head = "merged_okex_quotation"

        self.enable_data_pretreatment = True

    def filename_format(self, timestamp):
        date = datetime.fromtimestamp(timestamp / 1000, tz=UTC)
        return f"{self.data_file_head}_{date.strftime('%Y%m%d')}.h5"

    def snapshot_format(self, timestamp):
        date = datetime.fromtimestamp(timestamp / 1000, tz=UTC)
        return f"{self.snapshot_head}_{date.strftime('%Y%m%d')}.h5"

    def init_strategy(self, strategy_class, params: dict = None):
        self.strategy_class = strategy_class
        self.strategy = strategy_class()
        if not isinstance(params, dict):
            params = {}
        self.strategy.init(self.c2s_queue, self.delay_ms, params)

    def init_setting(self, data_root=".", delay_ms=0):
        self.data_root = data_root
        self.delay_ms = delay_ms

    @staticmethod
    def save_orderbook_cache(cache_file: str, orderbook: dict):
        data = orderbook.copy()
        data["asks"] = list(data["asks"].items())
        data["bids"] = list(data["bids"].items())
        with open(cache_file, "w") as f:
            json.dump(data, f, use_decimal=True)

    @staticmethod
    def load_orderbook_cache(cache_file: str):
        with open(cache_file, "r") as f:
            orderbook = json.load(f, use_decimal=True)
            orderbook["asks"] = SortedDict(orderbook["asks"])
            orderbook["bids"] = SortedDict(neg, orderbook["bids"])
            return orderbook

    def orderbook_cache_file(self, instrument_id: str, begin: int):
        return os.path.join(self.cache_dir, f"{self.data_file_head}_cache_{instrument_id}_{begin}.json")

    # def init_last_from_cache(self, symbol):
    #     self.strategy.ob_2_lag = copy.deepcopy(self._orderbooks[symbol])
    #     self.strategy.ob_2_lag_update_cash = None

    def init_partial(self, instrument_ids: list, begin: int, use_cached: bool = False):
        filename = os.path.join(self.data_root, self.filename_format(begin))
        if not os.path.exists(filename):
            raise FileNotFoundError(filename)
        if self.enable_data_pretreatment:
            log_origin_partial_depth(filename, instrument_ids)
        tfile = tables.File(filename, "r")
        rgroup = tfile.get_node("/")
        for instrument_id in instrument_ids:
            if instrument_id not in rgroup:
                tfile.close()
                raise ValueError(f"Data of {instrument_id} not found in {filename}")
            self._positions[instrument_id] = {1: 0, -1: 0}
            cache_file = self.orderbook_cache_file(instrument_id, begin)
            if os.path.isfile(cache_file):
                # print(f"use cached orderbook: {cache_file}")
                self._orderbooks[instrument_id] = self.load_orderbook_cache(cache_file)
                # self.init_last_from_cache(instrument_id)
                continue
            group = rgroup[instrument_id]
            # ensure_ptimearray(tfile, group)
            ptime = get_partial_time(group, begin)
            if ptime < begin:
                if use_cached:
                    if instrument_id in self._orderbooks \
                            and ("localtime" in self._orderbooks[instrument_id]) \
                            and (self._orderbooks[instrument_id]["localtime"] < ptime):
                        ptime = self._orderbooks[instrument_id]["localtime"]

                    else:
                        self._orderbooks[instrument_id] = {
                            "asks": SortedDict(),
                            "bids": SortedDict(neg)
                        }
                else:
                    self._orderbooks[instrument_id] = {
                        "asks": SortedDict(),
                        "bids": SortedDict(neg)
                    }

                sequence = slicedSequence(instrument_id, group["depth"], int(ptime), int(begin))
                for ltime, mtype, data in sequence:
                    if mtype == 1:
                        self._orderbooks[instrument_id] = generatePartial(data)
                    else:
                        updatePartial(self._orderbooks[instrument_id], data)
                self.save_orderbook_cache(cache_file, self._orderbooks[instrument_id])
                print(f"save orderbook cache: {cache_file}")

        tfile.close()

    def make_starting_points(self, instrument_id: str, timestamp_list: list):
        diff = 24 * 3600000
        timestampes = {}
        for ts in timestamp_list:
            timestampes.setdefault(ts - ts % diff, set()).add(ts)
        for date_begin, time_set in timestampes.items():
            self._orderbooks = {}
            for ts in sorted(time_set):
                print("Make cache orderbook for:", datetime.fromtimestamp(ts / 1000, tz=UTC).isoformat())
                self.init_partial([instrument_id], ts, True)

    def init_starting_points(self, instrument_id: str, date: str, step: int = 3600000):
        self.strategy = HFTemplate()
        date_begin = datetime.strptime(date, "%Y-%m-%d").replace(tzinfo=UTC).timestamp() * 1e3
        date_end = date_begin - date_begin % 24 * 3600000 + 24 * 3600000
        timestamp_list = list(range(int(date_begin), int(date_end), int(step)))
        self.make_starting_points(instrument_id, timestamp_list)

    def run(self, instrument_ids: list, begin: int, end: int, group_size=3600000, data_sources: List[DataSource]=None):
        if not self.snapshot_root:
            self.snapshot_root = self.data_root
        self.strategy.instrument_ids.extend(instrument_ids)
        self.strategy.onInit()
        for iid in instrument_ids:
            self.set_instrument_id(iid)
        sequence = []
        if not data_sources:
            data_sources = []
        for ds in data_sources:
            ds.prepare(begin, end, group_size)
        self.init_partial(instrument_ids, begin)
        for iid, orderbook in self._orderbooks.items():
            self.strategy.update_orderbook(iid, orderbook)
        while begin < end:
            group = {}
            filename = os.path.join(self.data_root, self.filename_format(begin))
            if self.use_snapshot:
                sfile = os.path.join(self.snapshot_root, self.snapshot_format(begin))
                if sfile:
                    group["snapshot_file"] = sfile
                else:
                    raise FileNotFoundError(filename)
            if os.path.exists(filename):
                group["raw_file"] = filename
                next_begin = begin - begin % (24 * 3600000) + 24 * 3600000

                if end <= next_begin:
                    # sequence.append((filename, (int(begin), int(end))))
                    group["range"] = (int(begin), int(end))
                    sequence.append(group)
                    break
                else:
                    # sequence.append((filename, (int(begin), int(next_begin))))
                    group["range"] = (int(begin), int(next_begin))
                    begin = next_begin
            else:
                raise FileNotFoundError(filename)

            sequence.append(group)

        print(sequence)

        if self.use_snapshot:
            for daily in sequence:
                filename, _range = daily["raw_file"], daily["range"]
                sfile = daily["snapshot_file"]
                snapshots = read_snapshot(sfile, instrument_ids)
                for _r, s in makeJoinedSequence(filename, instrument_ids, _range[0], _range[1], group_size):
                    # print(f"runtime: {datetime.fromtimestamp(_r[0]/1e3)}")
                    ss = slice_snapshot(snapshots, _r[0], _r[1])
                    self.run_loop(sorted(chain(*s, *ss), key=lambda tp: tp[0:2]))
        else:
            for daily in sequence:
                filename, _range = daily["raw_file"], daily["range"]
                for _r, s in makeJoinedSequence(filename, instrument_ids, _range[0], _range[1], group_size):
                    # print(f"runtime: {datetime.fromtimestamp(_r[0]/1e3)}")
                    for ds in data_sources:
                        s.append(self.iter_csutomized_sequence(ds, _r[0], _r[1]))

                    self.run_loop(sorted(chain(*s), key=lambda tp: tp[0:2]))

        self.close_on_stop(end)
        return self.strategy.onStop()
    
    def iter_csutomized_sequence(self, cds: DataSource, begin: int, end: int):
        for doc in cds.iter_data(begin, end):
            yield (doc["timestamp"], 4, doc)

    def current_timestamp(self):
        return self._current_time

    def next_operate_id(self):
        self._oid += 1
        return self._oid

    def next_trade_id(self):
        self._tid += 1
        return str(self._tid)

    def set_instrument_id(self, instrument_id):
        self._levelqueue[instrument_id] = {
            -1: SortedDict(),
            1: SortedDict(neg)
        }

    def _put_orderqueue(self, depths, order):
        instrument_id, side, price = order["instrument_id"], order["side"], order["price"]
        pos = depths[price] if price in depths else 0
        if price in self._levelqueue[instrument_id][side]:
            odqueue = self._levelqueue[instrument_id][side][price]
            odqueue.add_order(order, pos)
        else:
            odqueue = self.order_queue_class(order, pos)
            self._levelqueue[instrument_id][side][price] = odqueue

    # 主动成交，成交价为对手价
    def transaction_make(self, timestamp, depths, order):
        if order['match_type'] == OrderMatchType.PostOnly:
            canceled = False
            if order['side'] == -1 and depths.keys()[0] > order['price']:
                canceled = True
            if order['side'] == 1 and depths.keys()[0] < order['price']:
                canceled = True
            if canceled:
                order['status'] = OrderStatus.Canceled
                self.response_order(timestamp, order)
                self.trash_order(order['order_id'])
                return

        del_price = []
        for price in depths.irange(maximum=order["price"]):
            qty = depths[price]
            unfilled = order["qty"] - order['filled_qty']
            if qty >= unfilled:
                order["filled_qty"] += unfilled
                order["status"] = OrderStatus.Filled
                depths[price] -= unfilled
                self.make_trade(order, price, unfilled, "taker", timestamp)
                self.trash_order(order["order_id"])
                if depths[price] == 0:
                    del_price.append(price)
                break
            elif qty:
                order["filled_qty"] += qty
                self.make_trade(order, price, qty, "taker", timestamp)
                depths[price] = 0
                del_price.append(price)

        for price in del_price:
            del depths[price]

    def make_trade(self, order, price, qty, ttype, timestamp):
        tid = self.next_trade_id()
        trade = {
            "transaction_id": tid,
            "order_id": order["order_id"],
            "price": price,
            "qty": qty,
            "side": order["side"],
            "trade_type": ttype,
            "timestamp": timestamp,
            "order_type": order["order_type"],
            "instrument_id": order["instrument_id"]
        }
        order["trades"][trade["transaction_id"]] = trade
        order["price_avg"] = sum([trade["qty"] * trade["price"] for trade in order["trades"].values()]) / sum(
            [trade["qty"] for trade in order["trades"].values()])
        order["last_fill_id"] = tid
        side = order["side"]
        iid = order["instrument_id"]
        if order["order_type"] == OrderType.Open:
            factor = 1
        else:
            factor = -1
            side = -side
        self._positions[iid][side] += (trade["qty"] * factor)

        self.response_order(timestamp, order)

    def close_on_stop(self, timestamp):
        print("End position", self._positions)
        for iid, dpos in self._positions.items():
            orderbook = self._orderbooks[iid]
            if dpos[1] != 0:
                print(f"Unfinished position {iid}, side=1, qty={dpos[1]}")
                price, _qty = orderbook["asks"].peekitem(0)
                order = self.strategy.sendOrder(iid, OrderType.Close, -1, price, dpos[1])
                order["status"] = OrderStatus.Filled
                order["update_time"] = timestamp
                order["filled_qty"] = order["qty"]
                self.make_trade(order, price, dpos[1], "CloseOnEnd", timestamp)
                self._finished_order.append(order)
                self.strategy.onOrder(order.copy())

            if dpos[-1] > 0:
                print(f"Unfinished position {iid}, side=-1, qty={dpos[-1]}")
                price, _qty = orderbook["asks"].peekitem(0)
                order = self.strategy.sendOrder(iid, OrderType.Close, -1, price, dpos[-1])
                order["status"] = OrderStatus.Filled
                order["update_time"] = timestamp
                order["filled_qty"] = order["qty"]
                self.make_trade(order, price, dpos[-1], "CloseOnEnd", timestamp)
                self._finished_order.append(order)
                self.strategy.onOrder(order.copy())

    def response_order(self, timestamp, order):
        order["update_time"] = timestamp
        order = order.copy()
        self.s2c_queue.add(
            (timestamp + self.delay_ms, self.next_operate_id(), (RecieveMessageType.OrderStatusChanged, order)))

    def handle_s2c_queue(self, timestamp):
        while (self.s2c_queue):
            if self.s2c_queue[0][0] < timestamp:
                _stime, _oid, payload = self.s2c_queue.pop(0)
                msg_type, data = payload
                self.s2c_handlers[msg_type](data)

            else:
                break

    def handle_c2s_queue(self, timestamp):
        while (self.c2s_queue):
            if self.c2s_queue[0][0] < timestamp:
                _stime, _oid, payload = self.c2s_queue.pop(0)
                msg_type, data = payload
                self.c2s_handlers[msg_type](_stime, data)

            else:
                break

    # 交易所收到订单请求
    def on_send_order(self, timestamp, order):
        # 修改订单状态并返回挂单订单状态更新\
        order = order.copy()
        order["status"] = OrderStatus.Pending
        order["last_fill_id"] = ""
        self._orders[order["order_id"]] = order
        self.response_order(timestamp, order)

        # 获取当前订单同方向档口和反方向档口
        level_name = self.SIDE_MAP[order["side"]]
        anti_name = self.SIDE_MAP[-order["side"]]
        depths = self._orderbooks[order["instrument_id"]][level_name]
        anti = self._orderbooks[order["instrument_id"]][anti_name]

        # 主动成交
        self.transaction_make(timestamp, anti, order)

        # 扣除掉主动成交后剩余部分以原价位挂单
        if order["status"] not in FINISHED_STATUS:
            self._put_orderqueue(depths, order)

    def on_cancel_order(self, timestamp, order_id):
        order = self.trash_order(order_id)
        if not order:
            order = {"order_id": order_id, "status": OrderStatus.Error}
            self.s2c_queue.add(
                (timestamp + self.delay_ms, self.next_operate_id(), (RecieveMessageType.OrderStatusChanged, order)))
            return
        order["update_time"] = timestamp
        sd = self._levelqueue[order["instrument_id"]][order["side"]]
        if order["price"] in sd:
            oq = sd[order["price"]]
            canceld = oq.remove_order(order["order_id"])
            canceld["status"] = OrderStatus.Canceled
            if oq.is_empty():
                del sd[order["price"]]
        else:
            canceld = order
            canceld["status"] = OrderStatus.Error

        self.response_order(timestamp, canceld)

    def call_strategy_order(self, order):
        self.strategy.onOrder(order)

    def run_loop(self, sequence):
        for ltime, mtype, data in sequence:
            self._handlers[mtype](data)
            self.handle_c2s_queue(ltime)
            self.handle_s2c_queue(ltime)

    ORDERBOOK_QUEUE_SEQUENCE = (("asks", -1), ("bids", 1))

    def handle_snapshots(self, data):
        self.strategy.onSnapshot(data)

    # 收到orderbook update 执行挂单排队调整，并生成全量orderbook
    def handle_partial(self, data):
        for name, side in self.ORDERBOOK_QUEUE_SEQUENCE:
            levelqueue = self._levelqueue[data["instrument_id"]][side]
            levels = data[name]
            for price, qty in levels:
                if price in levelqueue:
                    levelqueue[price].on_depth_update(qty)

        partial = generatePartial(data)
        self._orderbooks[data["instrument_id"]] = partial
        self.strategy.update_orderbook(data["instrument_id"], partial)
        self.strategy.update_ts(data["localtime"])
        self.strategy.onL2Partial(data)

    # 收到orderbook update 执行挂单排队调整，并更新全量orderbook
    def handle_update(self, data):
        for name, side in self.ORDERBOOK_QUEUE_SEQUENCE:
            levelqueue = self._levelqueue[data["instrument_id"]][side]
            levels = data[name]
            for i in range(len(levels)):
                if levels[i][0] in levelqueue:
                    levelqueue[levels[i][0]].on_depth_update(levels[i][1])

        # updatePartial(self._orderbooks[data["instrument_id"]], data)
        updatePartialDiff(self._orderbooks[data["instrument_id"]], data)
        self.strategy.update_ts(data["localtime"])
        self.strategy.onL2Update(data)

    # 收到trade更新执行被动成交，成交价为挂单价
    def handle_trade(self, data):
        for levelqueues in self._levelqueue[data["instrument_id"]].values():
            qty = data["qty"]
            for price in list(levelqueues.irange(maximum=data["price"])):
                oq = levelqueues[price]
                for order, trade_qty in oq.on_trade(qty):
                    self.make_trade(order, price, trade_qty, "maker", data["localtime"])
                    if order["status"] in FINISHED_STATUS:
                        self.trash_order(order["order_id"])
                if oq.is_empty():
                    del levelqueues[price]
            self.strategy.update_ts(data["localtime"])
        self.strategy.onL2Trade(data)

    def trash_order(self, order_id):
        order = self._orders.pop(order_id, None)
        if order:
            self._finished_order.append(order)
        return order

    def make_trade_log(self):
        orders = []
        transactions = []
        for order in self._finished_order:
            for transaction in order["trades"].values():
                transactions.append(transaction)
            od = order.copy()
            od["transaction_ids"] = ",".join(od.pop("trades").keys())
            orders.append(od)
        order_columns = ["order_id", "instrument_id", "side", "order_type", "price", "qty", "price_avg", "filled_qty",
                         "status", "create_time", "update_time", "transaction_ids"]
        if len(orders):
            order_table = pd.DataFrame(orders, columns=order_columns).sort_values("create_time")
            for name in ["order_type", "status"]:
                order_table[name] = order_table[name].apply(lambda v: v.name)
        else:
            order_table = pd.DataFrame(columns=order_columns)
        transaction_columns = ["timestamp", "transaction_id", "order_id", "instrument_id", "side", "order_type",
                               "trade_type", "price", "qty"]
        if len(transactions):
            transaction_table = pd.DataFrame(transactions, columns=transaction_columns).sort_values("timestamp")
            transaction_table["order_type"] = transaction_table["order_type"].apply(lambda v: v.name)
        else:
            transaction_table = pd.DataFrame(columns=transaction_columns)
        return order_table, transaction_table

    def data_pretreatment(self, date: str, instrument_ids: list):
        date_begin = datetime.strptime(date, "%Y-%m-%d").replace(tzinfo=UTC).timestamp() * 1e3
        filename = os.path.join(self.data_root, self.filename_format(date_begin))
        if Path(filename).exists():
            log_origin_partial_depth(filename, instrument_ids)
            return True
        return False

    def handle_customized(self, data):
        self.strategy.onCustomizedData(data)
