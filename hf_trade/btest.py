import pandas as pd
import numpy as np
from datetime import datetime, timedelta, timezone
import backtest
from backtest import FINISHED_STATUS, OrderType, OrderStatus



class TestStrategy(backtest.HFTemplate):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.tcount = {}
        self.openOrder = {}
        self.closeOrder = {}
        self.orders = {}

    def onInit(self):
        for iid in self.instrument_ids:
            self.tcount[iid] = 0

    # 订单状态更新回调函数
    def onOrder(self, order):
        print("On order", order)
        oid = order["order_id"]
        # 订单不存在不进行后续处理
        if oid not in self.orders:
            print(f"order:{oid} not found.")
            return 
        
        # 该订单已完成不进行后续处理
        if self.orders[oid]["status"] in FINISHED_STATUS:
            print(f"order:{oid} already finished: {self.orders[oid]['status']}.")
            return 
        
        # 更新订单状态
        self.orders[order["order_id"]] = order
        iid = order["instrument_id"]

        # 平仓
        if order["order_type"] == OrderType.Close:
            self.closeOrder[iid] = order
            # 平仓单已完成则删除该订单和对应开仓
            if order["status"] in FINISHED_STATUS:
                oo = self.openOrder.pop(iid, None)
                co = self.closeOrder.pop(iid, None)
                if oo:
                    self.orders.pop(oo["order_id"], None)
                if co:
                    self.orders.pop(co["order_id"], None)
        # 开仓
        elif order["order_type"] == OrderType.Open:
            self.openOrder[iid] = order
            # 开仓被撤单则删除开仓
            if order["status"] == OrderStatus.Canceled:
                self.openOrder.pop(iid, None)
                self.orders.pop(order["order_id"], None)

    # 行情成交回调函数
    # 每个品种每100张成交下单一次，如果没有开仓则开仓，有开仓平仓，如果上一次下单未成交则撤单。
    def onL2Trade(self, trade):
        iid = trade["instrument_id"]
        self.tcount[iid] += 1
        if self.tcount[iid] % 100 == 0:
            depth = self.getOrderbook(iid)
            if iid not in self.openOrder:
                price, qty = depth["bids"].peekitem(1)
                order = self.sendOrder(
                    iid,
                    OrderType.Open,
                    1, 
                    price,
                    1
                )
                self.openOrder[iid] = order
                self.orders[order['order_id']] = order
                return 
            else:
                if self.openOrder[iid]["status"] != OrderStatus.Filled:
                    self.cancelOrder(self.openOrder[iid]["order_id"])
                    return
            if iid not in self.closeOrder:
                if self.openOrder[iid]["status"] == OrderStatus.Filled:
                    price, qty = depth["bids"].peekitem(1)
                    order = self.sendOrder(
                        iid,
                        OrderType.Close,
                        -1,
                        price,
                        1   
                    )
                    self.closeOrder[iid] = order
                    self.orders[order['order_id']] = order
            

def test_engine(instrument_ids: list, begin: int, end: int, root: str=".", delay_ms: int=0):

    # 品种，时间，backtest.UTC为预先定义号的utc时区类型。
    # instruments = ["btc_usd_cq", "eos_usd_cq"]
    # begin = datetime(2020, 3, 27, 1, tzinfo=backtest.UTC).timestamp()
    # end = datetime(2020, 3, 27, 2, tzinfo=backtest.UTC).timestamp()

    start = datetime.now()

    # 初始化回测
    bt = backtest.Backtest()

    # 设置单向时延
    bt.delay_ms = delay_ms
    # 设置数据路径为当前路径
    bt.data_root = root

    # 回测范围只包含20200326当天，因此当前目录下需要有文件okex_quotation_20200326.h5

    # 初始化策略类和参数
    bt.init_strategy(TestStrategy)

    # 运行策略，起止时间单位为毫秒
    bt.run(
        instrument_ids,
        int(begin),
        int(end)
    )
    print("run backtest", datetime.now()-start)

    # 生成订单和成交表输出到csv
    orders, transactions = bt.make_trade_log()
    orders.to_csv("orders_delay.csv")
    transactions.to_csv("transactions_delay.csv")


config = {
    "instrument_ids": ["btc_usd_cq", "eos_usd_cq"],
    "begin": int(datetime(2020, 3, 27, 1, tzinfo=backtest.UTC).timestamp()*1000),
    "end": int(datetime(2020, 3, 27, 2, tzinfo=backtest.UTC).timestamp()*1000)
}


import click


DT_FORMAT = "%Y-%m-%dT%H:%M:%S"

@click.command()
@click.option("-b", "--begin", type=str, help=f"timezone: utc, format: {DT_FORMAT}")
@click.option("-e", "--end", type=str, help=f"timezone: utc, format: {DT_FORMAT}")
@click.option("-d", "--delay", default=0, type=int, show_default=True)
@click.option("-r", "--root", default=".", type=str, show_default=True)
@click.argument("instrument_ids", nargs=-1)
def run(instrument_ids, begin, end, root, delay):
    if instrument_ids:
        config["instrument_ids"] = list(instrument_ids)
    if begin:
        config["begin"] = int(datetime.strptime(begin, DT_FORMAT).replace(tzinfo=backtest.UTC).timestamp()*1000)
    if end:
        config["end"] = int(datetime.strptime(end, DT_FORMAT).replace(tzinfo=backtest.UTC).timestamp()*1000)
    config["root"] = root
    config["delay_ms"] = delay
    print(config)
    test_engine(**config)
    

def main():
    # test_engine()
    run()


if __name__ == "__main__":
    main()