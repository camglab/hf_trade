import pandas as pd
from typing import List, Union
from datetime import datetime, timedelta, timezone, tzinfo
import os


class DataSource(object):

    def iter_data(self, begin: int, end: int):
        raise NotImplementedError()

    def prepare(self, begin: int, end: int, group_size: int):
        pass


class SingleCSVDataSource(DataSource):

    def __init__(self, path: str, tzinfo: timezone=timezone(timedelta(0))) -> None:
        super().__init__()
        self.path = path
        self.tzinfo = tzinfo
        self.data: pd.DataFrame = None

    def iter_data(self, begin: int, end: int):
        data = self.data.loc[begin:end]
        data["timestamp"] = data.index
        return data.to_dict("record")
    
    def prepare(self, begin: int, end: int, group_size: int):
        
        self.data = pd.read_csv(
            self.path, index_col="ts"
        ).rename(lambda t: datetime.fromisoformat(t).replace(tzinfo=self.tzinfo).timestamp()*1000).loc[begin:end]


class HDFDataSource(DataSource):

    def __init__(self, path: str, keys: List[str], columns: List[str]=None, lable: str="default") -> None:
        super().__init__()
        self.path = path
        self.table = pd.HDFStore(path, mode="r")
        self.keys = keys
        self.lable = lable
        self.columns = columns
    
    def close(self):
        self.table.close()

    @staticmethod
    def bi_search_index(table, value, side="right"):
        begin = 0
        end = table.nrows
        while(begin+1<end):
            mid = int((begin+end)/2)
            _v = table[mid][0]
            if _v < value:
                begin = mid
            elif _v > value:
                end = mid
            else:
                return mid
        if side == "left":
            
            return begin
        else:
            return end

    @staticmethod
    def search_sorted(store:pd.HDFStore, key: str, columns: Union[List[str], None], start, end):
        table = store.get_storer(key).table
        s_index = HDFDataSource.bi_search_index(table, start, 'right')
        e_index = HDFDataSource.bi_search_index(table, end, 'right')
        return store.select(key, start=s_index, stop=e_index, columns=columns)

    def iter_data(self, begin: int, end: int):
        for key in self.keys:
            data = self.search_sorted(self.table, key, self.columns, begin, end)
            data["timestamp"] = data.index
            data['key'] = key
            data["lable"] = self.lable
            yield from data.to_dict("record")

    def prepare(self, begin: int, end: int, group_size: int):
        pass


class TimeSliceHDFDataSource(DataSource):

    DAY_MIL_SECS = 24*3600*1000

    def __init__(self, root: str, keys: List[str], columns: List[str]=None, lable: str="default") -> None:
        super().__init__()
        self.root = root 
        self.keys = keys
        self.lable = lable
        self.columns = columns
        self.current_tag = ""
        self.ds: Union[HDFDataSource, None] = None
    
    def iter_data(self, begin: int, end: int):
        next_begin = begin - begin % self.DAY_MIL_SECS + self.DAY_MIL_SECS
        while end > next_begin:
            tag = self.tag_name(begin - begin % self.DAY_MIL_SECS)
            self.switch_file(tag)
            yield from self.ds.iter_data(begin, next_begin)
            begin = next_begin
            next_begin = begin - begin % self.DAY_MIL_SECS + self.DAY_MIL_SECS

        tag = self.tag_name(begin - begin % self.DAY_MIL_SECS)
        self.switch_file(tag)
        yield from self.ds.iter_data(begin, end)

    def prepare(self, begin: int, end: int, group_size: int):
        tag = self.tag_name(begin - begin % self.DAY_MIL_SECS)
        self.switch_file(tag)

    def switch_file(self, tag: str):
        if tag != self.current_tag:
            if isinstance(self.ds, HDFDataSource):
                self.ds.close()
            filename = os.path.join(self.root, f"{tag}.hdf")
            self.ds = HDFDataSource(
                filename,
                self.keys,
                self.columns,
                self.lable
            )

    @staticmethod
    def tag_name(ts: int):
        return datetime.fromtimestamp(ts/1000, timezone(timedelta(hours=0))).strftime("%Y%m%d")
    
    def write(self, key: str, data: pd.DataFrame):
        if not os.path.isdir(self.root):
            os.makedirs(self.root, exist_ok=True)
        for tag, frame in data.groupby(self.tag_name):
            filename = os.path.join(self.root, f"{tag}.hdf")
            store = pd.HDFStore(filename, mode="a")
            store.append(key, frame)
            store.close()