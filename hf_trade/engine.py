from sortedcontainers import SortedList, SortedDict
from operator import neg
import pandas as pd
import numpy as np
from datetime import datetime, timedelta, timezone
from enum import Enum
from hf_trade.data_source import DataSource
from typing import Dict, List, Union, Type
from functools import partial
from decimal import Decimal


UTC = timezone(timedelta())


class StopStrategy(Exception): pass


class EngineStreamType(Enum):

    snapshot = 0
    full = 1
    update = 2
    trade = 3
    customized = 4 


class SendMessageType(Enum):
    Order = 1
    Cancel = 2


class RecieveMessageType(Enum):
    OrderStatusChanged = 1
    Trade = 2


class OrderType(Enum):
    Open = 1
    Close = 2


class OrderMatchType(Enum):
    Limit = 'limit'
    PostOnly = 'post_only'


class OrderStatus(Enum):
    Error = -1
    New = 1
    Pending = 2
    Filled = 3
    Canceled = 4
    End = 5


FINISHED_STATUS = {OrderStatus.Filled, OrderStatus.Canceled, OrderStatus.Error}


def makeSortedDepth(sd, depths):
    for price, volume in depths:
        sd[price] = volume


# 根据orderbook分组生成全量快照
def generatePartial(orderbook):
    _ob = orderbook.copy()
    # 这里用SortedDict是因为深度数据的需要同时满足按值(价格)存取和按顺序(挡位)存取
    asks = SortedDict()
    bids = SortedDict(neg)
    makeSortedDepth(asks, orderbook["asks"])
    makeSortedDepth(bids, orderbook["bids"])
    _ob["asks"] = asks
    _ob["bids"] = bids
    return _ob


# 更新快照
def updatePartial(orderbook, updates):
    orderbook["timestamp"] = updates["timestamp"]
    orderbook["localtime"] = updates["localtime"]
    for name in ["asks", "bids"]:
        dlist = updates[name]
        sd = orderbook[name]
        for i in range(len(dlist)):
            if dlist[i][1]:

                sd[dlist[i][0]] = dlist[i][1]
            # 如果value为0，则删除深度
            else:
                if dlist[i][0] in sd:
                    del sd[dlist[i][0]]


# 快照替换为增量
def updatePartialDiff(orderbook, updates):
    orderbook["timestamp"] = updates["timestamp"]
    orderbook["localtime"] = updates["localtime"]
    for name in ["asks", "bids"]:
        dlist = updates[name]
        sd = orderbook[name]
        for i in range(len(dlist)):
            # 当前orderbook包含该深度
            if dlist[i][0] in sd:
                # 做差得增量
                # _a = dlist[i][1] - sd[dlist[i][0]]
                # 深度更新有量，则更新当前档口
                if dlist[i][1]:
                    sd[dlist[i][0]], dlist[i][1] = dlist[i][1], dlist[i][1] - sd[dlist[i][0]]
                # 深度更新无量，删除档口
                else:
                    dlist[i][1] = -sd[dlist[i][0]]
                    del sd[dlist[i][0]]
                # 深度更新量改为曾量
            # 当前orderbook不包含该深度(当前深度为0)， 全量即增量不用做差。
            else:
                # 深度更新有量，则更新当前档口
                if dlist[i][1]:
                    sd[dlist[i][0]] = dlist[i][1]


class HFTemplate(object):

    def __init__(self):
        self.instrument_ids = []
        self.c2s_queue = None
        self._oid = 0
        self._opid = 0
        self._delay_ms = 0
        self._current_time = 0
        self._orderbooks = {}

    def init(self, c2s_queue, delay_ms, params: dict):
        self.c2s_queue = c2s_queue
        self._delay_ms = delay_ms
        for key, value in params.items():
            setattr(self, key, value)

    def onInit(self):
        pass

    def onStop(self):
        pass

    def update_orderbook(self, instrument_id, orderbook):
        self._orderbooks[instrument_id] = orderbook

    def current_time(self):
        return self._current_time

    def next_order_id(self):
        self._oid += 1
        return str(self._oid)

    def next_operate_id(self):
        self._opid += 1
        return self._opid

    def _send_order(self, order):
        self.c2s_queue.add(
            (self.current_time() + self._delay_ms, self.next_operate_id(), (SendMessageType.Order, order)))

    def _make_order(self, instrument_id, order_type, side, price, qty):
        oid = self.next_order_id()
        order = {
            "order_id": oid,
            "price": price,
            "side": side,
            "instrument_id": instrument_id,
            "order_type": order_type,
            "qty": qty,
            "filled_qty": 0,
            "trades": {},
            "create_time": self.current_time(),
            "update_time": 0
        }

        self._send_order(order)
        return order

    def onPosition(self, position):
        pass

    def onOrder(self, order):
        pass

    def sendOrder(
            self, instrument_id: str, order_type: OrderType, side: int, 
            price: Union[Decimal, float], qty: Union[Decimal, float], 
            match_type: OrderMatchType = OrderMatchType.Limit, 
            tag: str=""
        ) -> dict:
        """[summary]

        :param instrument_id: 品种
        :type instrument_id: str
        :param order_type: 发单类型(开平)
        :type order_type: OrderType
        :param side: 方向，buy: 1, sell: -1
        :type side: int
        :param price: 价格
        :type price: Union[Decimal, float]
        :param qty: 单量
        :type qty: Union[Decimal, float]
        :param match_type: 成交条件, defaults to OrderMatchType.Limit
        :type match_type: OrderMatchType, optional
        :param tag: 订单标记, defaults to ""
        :type tag: str, optional
        :return: 订单信息
        :rtype: dict
        """
        oid = self.next_order_id()
        order = {
            "order_id": oid,
            "price": price,
            "side": side,
            "instrument_id": instrument_id,
            "order_type": order_type,
            "qty": qty,
            "filled_qty": 0,
            "trades": {},
            "create_time": self.current_time(),
            "update_time": 0,
            "status": OrderStatus.New,
            'match_type': match_type,
            "tag": tag
        }
        self._send_order(order)
        return order

    def cancelOrder(self, order_id):
        self.c2s_queue.add(
            (self.current_time() + self._delay_ms, self.next_operate_id(), (SendMessageType.Cancel, order_id)))

    def update_ts(self, timestamp):
        self._current_time = timestamp

    def onL2Partial(self, depth):
        pass

    def onL2Update(self, depth):
        pass

    def onL2Trade(self, trade):
        pass

    def onSnapshot(self, snapshot):
        pass

    def onCustomizedData(self, data):
        pass

    def getOrderbook(self, instrument_id):
        return self._orderbooks[instrument_id]
    
    def sendMessage(self, key: str, value: str):
        """发送信息，在实时环境下调用可以向外部发送信息，回测该方法不执行任何操作。

        redis作为外部输出的情况下执行：set key value

        :param key: 
        :type key: str
        :param value: 
        :type value: str
        """
        pass

    def output(self, key: str, value: dict):
        """输出信息，在实时环境下调用向外部输出结构化信息，回测该方法不执行任何操作。

        实时下目前支持输出到hdf文件

        :param key: 输出目标，hdf模式下格式为："filetag/tablename"
        :type key: str
        :param value: 输出数据
        :type value: dict
        """
        pass
    
    def stop_and_raise(self, message: str):
        """策略抛出异常并主动退出

        :param message: 抛出异常时附带的信息
        :type message: str
        :raises StopStrategy: [description]
        """
        raise StopStrategy(message)


class OrderQueue(object):

    def __init__(self, order, pos=0):
        self._orders = {
            order["order_id"]: {
                "order": order,
                "next": None,
                "pre": None,
                "shift": 0
            }
        }
        self._front = order["order_id"]
        self._rear = order["order_id"]
        self.front_pos = pos
        self.rear_pos = pos + order["qty"]

    def add_order(self, order, pos=0):
        self._orders[self._rear]["next"] = order["order_id"]
        orderpos = max(pos, self.rear_pos)
        pack = {
            "order": order,
            "next": None,
            "pre": self._rear,
            "shift": orderpos - self.rear_pos
        }
        self._orders[order["order_id"]] = pack
        self._rear = order["order_id"]
        self.rear_pos = orderpos + order["qty"]

    def on_depth_update(self, qty):
        pack = self._orders[self._front]
        pos = self.front_pos
        if pos >= qty:
            self.front_pos = qty
            return

        while (pos < qty):
            pos += pack["order"]["qty"]
            if pack["next"]:
                pack = self._orders[pack["next"]]
                pos += pack["shift"]
            else:
                return

        pack["shift"] = max(pack["shift"] - pos + qty, 0)

    def _on_trade(self, qty):
        if self.front_pos:
            if qty <= self.front_pos:
                self.front_pos -= qty
                return
            else:
                qty = qty - self.front_pos
                self.front_pos = 0
        order = self.get_head()
        unfilled = order["qty"] - order["filled_qty"]

        # 可以完全成交
        if qty >= unfilled:
            qty = qty - unfilled
            order["filled_qty"] += unfilled
            order["status"] = OrderStatus.Filled
            self.pop_head()
            yield order, unfilled
            if qty and self.front_pos:
                yield from self._on_trade(qty)
        # 不能完全成交
        else:
            order["filled_qty"] += unfilled - qty
            yield order, unfilled - qty

    def on_trade(self, qty):
        return list(self._on_trade(qty))

    def get_head(self):
        return self._orders[self._front]["order"]

    def pop_head(self):
        order_id = self._front
        pack = self._orders.pop(order_id)
        self._front = pack["next"]
        if self._front:
            front = self._orders[self._front]
            self.front_pos += front["shift"]
            front["shift"] = 0
            front["pre"] = None
        else:
            self._rear = None
            self.rear_pos = 0
        return pack["order"]

    def remove_order(self, order_id):
        pack = self._orders.pop(order_id)
        next_id = pack["next"]
        pre_id = pack["pre"]
        if next_id:
            self._orders[next_id]["pre"] = pre_id
            self._orders[next_id]["shift"] += pack["shift"]
        else:
            self._rear = pre_id

        if pre_id:
            self._orders[pre_id]["next"] = next_id
        else:
            self._front = next_id

        return pack["order"]

    def is_empty(self):
        return not bool(self._orders)

    def show(self):
        print(f"front = {self.front_pos}\nrear  = {self.rear_pos}\ncount = {len(self._orders)}")
        if not self._front:
            print("() []")
            return
        pack = self._orders[self._front]
        print(f"({self.front_pos})", end=" ")
        print(f"[{pack['order']['qty']} {pack['order']['filled_qty']}]")
        while (pack["next"]):
            pack = self._orders[pack["next"]]
            print(f"({pack['shift']})", end=" ")
            print(f"[{pack['order']['qty']} {pack['order']['filled_qty']}]")


class PriorityOrderQueue(OrderQueue):

    def __init__(self, order, pos=0):
        super().__init__(
            order,
            max(pos - order["qty"], 0)
        )

    def on_depth_update(self, qty):
        pack = self._orders[self._front]
        super().on_depth_update(
            max(qty - pack["order"]["qty"], 0)
        )


class TransactionEngine(object):
    SIDE_MAP = {
        1: "bids",
        -1: "asks"
    }

    def __init__(self):
        self._orderbooks = {}
        self._orders = {}
        self._levelqueue = {}
        self._finished_order = []
        self._handlers = {
            EngineStreamType.snapshot.value: self.handle_snapshots,
            EngineStreamType.full.value: self.handle_partial,
            EngineStreamType.update.value: self.handle_update,
            EngineStreamType.trade.value: self.handle_trade,
            EngineStreamType.customized.value: self.handle_customized
        }
        self.strategy_class = None
        self.strategy = None
        self._oid = 0
        self._tid = 0

        self._current_time = 0
        self.delay_ms = 0

        self.time_tag = "localtime"

        self.c2s_queue = SortedList(key=lambda item: item[:2])
        self.s2c_queue = SortedList(key=lambda item: item[:2])

        self.c2s_handlers = {
            SendMessageType.Order: self.on_send_order,
            SendMessageType.Cancel: self.on_cancel_order,
        }

        self.s2c_handlers = {
            RecieveMessageType.OrderStatusChanged: self.call_strategy_order
        }

        self._positions = {}

        self.order_queue_class = OrderQueue

    def init_strategy(self, strategy_class: Type[HFTemplate], params: Dict = None):
        self.strategy_class = strategy_class
        self.strategy = strategy_class()
        if not isinstance(params, dict):
            params = {}
        self.strategy.init(self.c2s_queue, self.delay_ms, params)

    def init_portfolio(self, instrument_ids: List[str]):
        for instrument_id in instrument_ids:
            self._positions[instrument_id] = {1: 0, -1: 0}

    def init_orderbooks(self, orderbooks: dict):
        self._orderbooks.update(orderbooks)

    def current_timestamp(self):
        return self._current_time

    def next_operate_id(self):
        self._oid += 1
        return self._oid

    def next_trade_id(self):
        self._tid += 1
        return str(self._tid)

    def set_instrument_id(self, instrument_id):
        self._levelqueue[instrument_id] = {
            -1: SortedDict(),
            1: SortedDict(neg)
        }

    def _put_orderqueue(self, depths, order):
        instrument_id, side, price = order["instrument_id"], order["side"], order["price"]
        pos = depths[price] if price in depths else 0
        if price in self._levelqueue[instrument_id][side]:
            odqueue = self._levelqueue[instrument_id][side][price]
            odqueue.add_order(order, pos)
        else:
            odqueue = self.order_queue_class(order, pos)
            self._levelqueue[instrument_id][side][price] = odqueue

    # 主动成交，成交价为对手价
    def transaction_make(self, timestamp, depths, order):
        if order['match_type'] == OrderMatchType.PostOnly:
            canceled = False
            if order['side'] == -1 and depths.keys()[0] >= order['price']:
                canceled = True
            if order['side'] == 1 and depths.keys()[0] <= order['price']:
                canceled = True
            if canceled:
                order['status'] = OrderStatus.Canceled
                self.response_order(timestamp, order)
                self.trash_order(order['order_id'])
                return

        del_price = []
        for price in depths.irange(maximum=order["price"]):
            qty = depths[price]
            unfilled = order["qty"] - order['filled_qty']
            if qty >= unfilled:
                order["filled_qty"] += unfilled
                order["status"] = OrderStatus.Filled
                depths[price] -= unfilled
                self.make_trade(order, price, unfilled, "taker", timestamp)
                self.trash_order(order["order_id"])
                if depths[price] == 0:
                    del_price.append(price)
                break
            elif qty:
                order["filled_qty"] += qty
                self.make_trade(order, price, qty, "taker", timestamp)
                depths[price] = 0
                del_price.append(price)

        for price in del_price:
            del depths[price]

    def make_trade(self, order, price, qty, ttype, timestamp):
        tid = self.next_trade_id()
        trade = {
            "transaction_id": tid,
            "order_id": order["order_id"],
            "price": price,
            "qty": qty,
            "side": order["side"],
            "trade_type": ttype,
            "timestamp": timestamp,
            "order_type": order["order_type"],
            "instrument_id": order["instrument_id"]
        }
        order["trades"][trade["transaction_id"]] = trade
        order["price_avg"] = sum([trade["qty"] * trade["price"] for trade in order["trades"].values()]) / sum(
            [trade["qty"] for trade in order["trades"].values()])
        order["last_fill_id"] = tid
        side = order["side"]
        iid = order["instrument_id"]
        if order["order_type"] == OrderType.Open:
            factor = 1
        else:
            factor = -1
            side = -side
        self._positions[iid][side] += (trade["qty"] * factor)

        self.response_order(timestamp, order)

    def close_on_stop(self, timestamp):
        print("End position", self._positions)
        for iid, dpos in self._positions.items():
            orderbook = self._orderbooks[iid]
            if dpos[1] != 0:
                print(f"Unfinished position {iid}, side=1, qty={dpos[1]}")
                price, _qty = orderbook["asks"].peekitem(0)
                order = self.strategy.sendOrder(iid, OrderType.Close, -1, price, dpos[1])
                order["status"] = OrderStatus.Filled
                order["update_time"] = timestamp
                order["filled_qty"] = order["qty"]
                self.make_trade(order, price, dpos[1], "CloseOnEnd", timestamp)
                self._finished_order.append(order)
                self.strategy.onOrder(order.copy())

            if dpos[-1] > 0:
                print(f"Unfinished position {iid}, side=-1, qty={dpos[-1]}")
                price, _qty = orderbook["asks"].peekitem(0)
                order = self.strategy.sendOrder(iid, OrderType.Close, -1, price, dpos[-1])
                order["status"] = OrderStatus.Filled
                order["update_time"] = timestamp
                order["filled_qty"] = order["qty"]
                self.make_trade(order, price, dpos[-1], "CloseOnEnd", timestamp)
                self._finished_order.append(order)
                self.strategy.onOrder(order.copy())

    def response_order(self, timestamp, order):
        order["update_time"] = timestamp
        order = order.copy()
        self.s2c_queue.add(
            (timestamp + self.delay_ms, self.next_operate_id(), (RecieveMessageType.OrderStatusChanged, order)))

    def handle_s2c_queue(self, timestamp):
        while (self.s2c_queue):
            if self.s2c_queue[0][0] < timestamp:
                _stime, _oid, payload = self.s2c_queue.pop(0)
                msg_type, data = payload
                self.s2c_handlers[msg_type](data)

            else:
                break

    def handle_c2s_queue(self, timestamp):
        while (self.c2s_queue):
            if self.c2s_queue[0][0] < timestamp:
                _stime, _oid, payload = self.c2s_queue.pop(0)
                msg_type, data = payload
                self.c2s_handlers[msg_type](_stime, data)

            else:
                break

    # 交易所收到订单请求
    def on_send_order(self, timestamp, order):
        # 修改订单状态并返回挂单订单状态更新\
        order = order.copy()
        order["status"] = OrderStatus.Pending
        order["last_fill_id"] = ""
        self._orders[order["order_id"]] = order
        self.response_order(timestamp, order)

        # 获取当前订单同方向档口和反方向档口
        level_name = self.SIDE_MAP[order["side"]]
        anti_name = self.SIDE_MAP[-order["side"]]
        depths = self._orderbooks[order["instrument_id"]][level_name]
        anti = self._orderbooks[order["instrument_id"]][anti_name]

        # 主动成交
        self.transaction_make(timestamp, anti, order)

        # 扣除掉主动成交后剩余部分以原价位挂单
        if order["status"] not in FINISHED_STATUS:
            self._put_orderqueue(depths, order)

    def on_cancel_order(self, timestamp, order_id):
        order = self.trash_order(order_id)
        if not order:
            order = {"order_id": order_id, "status": OrderStatus.Error}
            self.s2c_queue.add(
                (timestamp + self.delay_ms, self.next_operate_id(), (RecieveMessageType.OrderStatusChanged, order)))
            return
        order["update_time"] = timestamp
        sd = self._levelqueue[order["instrument_id"]][order["side"]]
        if order["price"] in sd:
            oq = sd[order["price"]]
            canceld = oq.remove_order(order["order_id"])
            canceld["status"] = OrderStatus.Canceled
            if oq.is_empty():
                del sd[order["price"]]
        else:
            canceld = order
            canceld["status"] = OrderStatus.Error

        self.response_order(timestamp, canceld)

    def call_strategy_order(self, order):
        self.strategy.onOrder(order)

    def run_loop(self, sequence):
        for ltime, mtype, data in sequence:
            self.on_stream(ltime, mtype, data)

    def on_stream(self, ts, mtype, data):
        self._handlers[mtype](data)
        self.handle_c2s_queue(ts)
        self.handle_s2c_queue(ts)

    ORDERBOOK_QUEUE_SEQUENCE = (("asks", -1), ("bids", 1))

    def handle_snapshots(self, data):
        self.strategy.onSnapshot(data)

    # 收到orderbook update 执行挂单排队调整，并生成全量orderbook
    def handle_partial(self, data):
        for name, side in self.ORDERBOOK_QUEUE_SEQUENCE:
            levelqueue = self._levelqueue[data["instrument_id"]][side]
            levels = data[name]
            for price, qty in levels:
                if price in levelqueue:
                    levelqueue[price].on_depth_update(qty)

        partial = generatePartial(data)
        self._orderbooks[data["instrument_id"]] = partial
        self.strategy.update_orderbook(data["instrument_id"], partial)
        self.strategy.update_ts(data[self.time_tag])
        self.strategy.onL2Partial(data)

    # 收到orderbook update 执行挂单排队调整，并更新全量orderbook
    def handle_update(self, data):
        for name, side in self.ORDERBOOK_QUEUE_SEQUENCE:
            levelqueue = self._levelqueue[data["instrument_id"]][side]
            levels = data[name]
            for i in range(len(levels)):
                if levels[i][0] in levelqueue:
                    levelqueue[levels[i][0]].on_depth_update(levels[i][1])

        # updatePartial(self._orderbooks[data["instrument_id"]], data)
        updatePartialDiff(self._orderbooks[data["instrument_id"]], data)
        self.strategy.update_ts(data[self.time_tag])
        self.strategy.onL2Update(data)

    # 收到trade更新执行被动成交，成交价为挂单价
    def handle_trade(self, data):
        for levelqueues in self._levelqueue[data["instrument_id"]].values():
            qty = data["qty"]
            for price in list(levelqueues.irange(maximum=data["price"])):
                oq = levelqueues[price]
                for order, trade_qty in oq.on_trade(qty):
                    self.make_trade(order, price, trade_qty, "maker", data[self.time_tag])
                    if order["status"] in FINISHED_STATUS:
                        self.trash_order(order["order_id"])
                if oq.is_empty():
                    del levelqueues[price]
            self.strategy.update_ts(data[self.time_tag])
        self.strategy.onL2Trade(data)

    def trash_order(self, order_id):
        order = self._orders.pop(order_id, None)
        if order:
            self._finished_order.append(order)
        return order

    def make_trade_log(self):
        orders = []
        transactions = []
        for order in self._finished_order:
            for transaction in order["trades"].values():
                transactions.append(transaction)
            od = order.copy()
            od["transaction_ids"] = ",".join(od.pop("trades").keys())
            orders.append(od)
        order_columns = ["order_id", "instrument_id", "side", "order_type", "price", "qty", "price_avg", "filled_qty",
                         "status", "create_time", "update_time", "transaction_ids", "tag"]
        if len(orders):
            order_table = pd.DataFrame(orders, columns=order_columns).sort_values("create_time")
            for name in ["order_type", "status"]:
                order_table[name] = order_table[name].apply(lambda v: v.name)
        else:
            order_table = pd.DataFrame(columns=order_columns)
        transaction_columns = ["timestamp", "transaction_id", "order_id", "instrument_id", "side", "order_type",
                               "trade_type", "price", "qty"]
        if len(transactions):
            transaction_table = pd.DataFrame(transactions, columns=transaction_columns).sort_values("timestamp")
            transaction_table["order_type"] = transaction_table["order_type"].apply(lambda v: v.name)
        else:
            transaction_table = pd.DataFrame(columns=transaction_columns)
        return order_table, transaction_table

    def handle_customized(self, data):
        self.strategy.onCustomizedData(data)

    def init_data(self, instrument_ids: List[str], orderbooks: dict):
        for instrument_id in instrument_ids:
            self.set_instrument_id(instrument_id)
        self.init_orderbooks(orderbooks)
        self.init_portfolio(instrument_ids)
        self.strategy.instrument_ids.extend(instrument_ids)
        self.strategy.onInit()
        for iid, orderbook in self._orderbooks.items():
            self.strategy.update_orderbook(iid, orderbook)