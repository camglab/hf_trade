import tables
from typing import List, Dict
import os
from datetime import datetime, timezone, timedelta


UTC = timezone(timedelta())


def make_description(name: str, conf: List[List[str]]) -> tables.IsDescription.__class__:
    columns = {}
    for idx, item in enumerate(conf):
        colname, coltype = item
        if coltype.startswith("String"):
            itemsize = int(coltype[6:])
            columns[colname] = tables.StringCol(itemsize, pos=idx)
        else:
            columns[colname] = tables.Col._subclass_from_prefix(coltype)(pos=idx)
        
    return type(
        name, 
        (tables.IsDescription,),
        columns
    )


class TableLogger(object):

    def __init__(self, filename: str, table_configs: Dict[str, List[List[str]]]):
        self.filename = filename
        self.file = tables.File(filename, "a")
        self.table_configs = table_configs
        self.tables: Dict[str, tables.Table] = {}
        for table_name, table_conf in self.table_configs.items():
            node_name = f"/{table_name}"
            if node_name not in self.file:
                DescriptionClass = make_description(table_name, table_conf)
                self.tables[table_name]: tables.Table = self.file.create_table("/", table_name, DescriptionClass)
                self.file.flush()
            else:
                self.tables[table_name]: tables.Table = self.file.get_node(node_name)
    
    def __enter__(self):
        return self
    
    def __exit__(self, *args):
        self.close()

    def flush(self):
        self.file.flush()

    def close(self):
        if self.file.isopen:
            self.file.close()
        
    def on_data(self, table_name: str, records: List[Dict]):
        config = self.table_configs[table_name]
        docs = [
            tuple(record[name] for name, _ in config) 
            for record in records
        ]
        table: tables.Table = self.tables[table_name]
        table.append(docs)
    
    def switch(self):
        pass

    
class RotateTableLogger(TableLogger):

    def __init__(self, log_dir: str, filename_head: str, table_configs: Dict[str, List[List[str]]]):
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)
        self.log_dir = log_dir
        self.filename_head = filename_head
        self.date = datetime.now(tz=UTC).date()
        super().__init__(self.current_filename(), table_configs)

    def current_filename(self):
        dt = self.date.strftime("%Y%m%d")
        return os.path.join(self.log_dir, f"{self.filename_head}_{dt}.h5") 

    def switch(self):
        date = datetime.now(tz=UTC).date()
        if date != self.date:
            self.close()
            self.date = date
            super().__init__(self.current_filename(), self.table_configs)