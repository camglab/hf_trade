import tables
import pandas as pd
from datetime import datetime, timedelta, timezone
from sortedcontainers import SortedList, SortedDict
import simplejson as json
from operator import neg
import numpy as np
import os
from functools import partial
from itertools import chain, groupby 
from typing import Any, List, Tuple, Dict, Iterable, Optional, Type
from hf_trade.engine import HFTemplate, TransactionEngine
from hf_trade.data_source import DataSource


UTC = timezone(timedelta())

# 更新快照
def updatePartial(orderbook, updates):
    orderbook["timestamp"] = updates["timestamp"]
    orderbook["localtime"] = updates["localtime"]
    for name in ["asks", "bids"]:
        dlist = updates[name]
        sd = orderbook[name]
        for i in range(len(dlist)):
            if dlist[i][1]:

                sd[dlist[i][0]] = dlist[i][1]
            # 如果value为0，则删除深度
            else:
                if dlist[i][0] in sd:
                    del sd[dlist[i][0]]


# 快照替换为增量
def updatePartialDiff(orderbook, updates):
    orderbook["timestamp"] = updates["timestamp"]
    orderbook["localtime"] = updates["localtime"]
    for name in ["asks", "bids"]:
        dlist = updates[name]
        sd = orderbook[name]
        for i in range(len(dlist)):
            # 当前orderbook包含该深度
            if dlist[i][0] in sd:
                # 做差得增量
                # _a = dlist[i][1] - sd[dlist[i][0]]
                # 深度更新有量，则更新当前档口
                if dlist[i][1]:
                    sd[dlist[i][0]], dlist[i][1] = dlist[i][1], dlist[i][1] - sd[dlist[i][0]]
                # 深度更新无量，删除档口
                else:
                    dlist[i][1] = -sd[dlist[i][0]]
                    del sd[dlist[i][0]]
                # 深度更新量改为曾量
            # 当前orderbook不包含该深度(当前深度为0)， 全量即增量不用做差。
            else:
                # 深度更新有量，则更新当前档口
                if dlist[i][1]:
                    sd[dlist[i][0]] = dlist[i][1]


def find_partial_time(node):
    p = node.read_where("action == 1", field="localtime")
    return np.array(sorted(set(p)))


def make_partial_log(tfile, group):
    array = find_partial_time(group["depth"])
    node = tfile.create_array(group, "ptime", array)
    node.flush()
    return node


def ensure_ptimearray(tfile, group):
    if "depth" in group and ("ptime" not in group):
        print(f"make partial timelist {group._v_name}")
        make_partial_log(tfile, group)


def log_origin_partial_depth(filename: str, instrument_ids: list):
    print(f"[log origin partial] {filename} {instrument_ids}")
    tfile = tables.File(filename, "a")
    rgroup = tfile.get_node("/")
    for iid in instrument_ids:
        ensure_ptimearray(tfile, rgroup[iid])
    tfile.close()


def generate_ptimearray(tfile):
    for group in tfile.list_nodes("/"):
        ensure_ptimearray(tfile, group)


def get_partial_time(group, begin_time):
    _a = group["ptime"][:]
    pos = _a.searchsorted(begin_time, "right") - 1
    if pos < 0:
        pos = 0
    return _a[pos]


# 聚合迭代增量数据
# 合并增量数据不需要逐个计算，在存储的时候同一组数据是是连在一起的，通过计算得到的边界值批处理可以大幅加速。
def iterOrderBookSequence(instrument_id: str, depth: pd.DataFrame, time_tag="localtime") -> Iterable[Tuple[int, int, Dict[str, Any]]]:
    depth["index"] = depth.index
    # 按id分组得到每组的action和localtime,timestamp
    joined = depth.groupby("id")[["action", "localtime", "timestamp"]].first()
    # 按id和msg_type分组得到每组的ask和bid的起止地址
    table = depth.groupby(["id", "msg_type"])["index"].agg(**{"begin": "first", "count": "count"})
    table["end"] = table["begin"] + table["count"]
    data = table.reset_index().pivot("id", "msg_type", ["begin", "end"]).sort_index(1, [1, 0]).fillna(0)
    data["action"] = joined.action
    data["localtime"] = joined.localtime
    data["timestamp"] = joined["timestamp"]
    # data = data.reset_index()
    # 将两表合并后可以得到每次增量的timestamp localtime action ask起止地址 bid起止地址

    # 这里用nparray截取速度更快
    dvalues = depth.values
    if len(data.columns) != 7:
        data = pd.DataFrame(
            data,
            columns=pd.MultiIndex(
                [['begin', 'end', 'action', 'localtime', 'timestamp'], [1, 2, '']],
                [[0, 1, 0, 1, 2, 3, 4], [0, 0, 1, 1, 2, 2, 2]],
                names=[None, 'msg_type']
            )
        ).fillna(0)

    for bb, be, ab, ae, action, localtime, timestamp in data.values:
        ltime = int(localtime)
        _d = {
            "localtime": int(localtime),
            "timestamp": int(timestamp),
            "asks": dvalues[int(ab):int(ae), 3:5],
            "bids": dvalues[int(bb):int(be), 3:5],
            "instrument_id": instrument_id
        }
        # yield 返回分组增量。
        yield (_d[time_tag], int(action), _d)


side_map = {
    1: 1,
    2: -1,
}


# 处理并返回trade数据        
def iterTradeSequence(instrument_id: str, frame: pd.DataFrame, time_tag="localtime"):
    # 这里没有迭代pandas而是迭代np数组所以速度快很多
    for row in frame.values:
        _t = {
            "trade_id": str(row[1]),
            "price": row[2],
            "qty": row[3],
            "side": side_map[int(row[4])],
            "localtime": int(row[5]),
            "timestamp": int(row[6]),
            "instrument_id": instrument_id
        }
        yield (_t[time_tag], 3, _t)


# trade和depth增量按时间聚合排序
def genSequence(instrument_id: str, trade: pd.DataFrame, orderbook: pd.DataFrame):
    return sorted(chain(iterOrderBookSequence(instrument_id, orderbook), iterTradeSequence(instrument_id, trade)),
                  key=lambda tp: tp[0])


def slicedSequence(instrument_id, table, begin, end, group_size=3600000):
    pf = get_price_factor(symbol=instrument_id)
    qf = get_qty_factor(instrument_id)
    timelist = list(range(begin, end, group_size))
    if timelist[-1] < end:
        timelist.append(end)
    dtime = table.col("localtime")
    for i in range(len(timelist) - 1):
        bpos = dtime.searchsorted(timelist[i])
        epos = dtime.searchsorted(timelist[i + 1])
        data = pd.DataFrame(table[bpos:epos])
        adjust_data(data, ADJUST_TYPE, pf, qf)

        yield from iterOrderBookSequence(instrument_id, data)


from decimal import Decimal


ADJUST_TYPE = "decimal"


PFACTOR = Decimal(1e4)


def adjustPrice(price, factor=PFACTOR):
    return Decimal(price) / factor


PRICE_FACTOR = {
    'btc': Decimal(1e3),
    'yfi': Decimal(1e3),
    'yfii': Decimal(1e3),
    'eth': Decimal(1e4),
    'trx': Decimal(1e10)
}


QTY_FACTOR = {}


def get_price_factor(symbol):
    base, *_ = symbol.split('_')
    return PRICE_FACTOR.get(base, Decimal(1e5))


def get_qty_factor(symbol):
    base, *_ = symbol.split('_')
    return QTY_FACTOR.get(base, Decimal(1))


def adjustQty(qty):
    return Decimal(str(qty))


def adjust_data(data: pd.DataFrame, adjust_type: str, price_factor: Decimal = Decimal(1), qty_factor: Decimal = Decimal(1)):
    if adjust_type == "decimal":
        data["price"] = data["price"].apply(partial(adjustPrice, factor=price_factor))
        data["qty"] = data["qty"].apply(adjustQty)
    elif adjust_type == "float":
        data["price"] = data["price"] / float(price_factor)
    elif adjust_type == "int":
        pf = float(price_factor)
        qf = float(qty_factor)
        data["price"] = data["price"].apply(lambda x: int(x/pf))
        data["qty"] = data["qty"].apply(lambda x: int(x*qf))


def makeSortedDepth(sd, depths):
    for price, volume in depths:
        sd[price] = volume

    
# 根据orderbook分组生成全量快照
def generatePartial(orderbook):
    _ob = orderbook.copy()
    # 这里用SortedDict是因为深度数据的需要同时满足按值(价格)存取和按顺序(挡位)存取
    asks = SortedDict()
    bids = SortedDict(neg)
    makeSortedDepth(asks, orderbook["asks"])
    makeSortedDepth(bids, orderbook["bids"])
    _ob["asks"] = asks
    _ob["bids"] = bids
    return _ob


class HDFQuotation(object):

    def __init__(self, filename: str, instrument_tag: str = ""):
        self.filename: str = filename
        self.instrument_tag = instrument_tag
    
    def get_index(self, instrument_ids: List[str], points: List[int]) -> Dict[str, Dict[str, List[int]]]:
        indexes = {}
        with tables.open_file(self.filename, "r") as tfile:
            for instrument_id in instrument_ids:
                dname = f"/{instrument_id}/depth"
                dnode = tfile.get_node(dname)
                dtime = dnode.col("localtime")
                tname = f"/{instrument_id}/trade"
                tnode = tfile.get_node(tname)
                ttime = tnode.col("localtime") 
                indexes[instrument_id] = {
                    "depth": [dtime.searchsorted(t) for t in points],
                    "trade": [ttime.searchsorted(t) for t in points]
                }
        return indexes
    
    def batch_read(self, depths: List[Tuple[str, int, int]], trades: List[Tuple[str, int, int]], time_tag="localtime") -> Iterable[Iterable[Tuple[int, int, Dict]]]:
        with tables.open_file(self.filename, "r") as tfile:
            for instrument_id, begin, end in depths:
                pf = get_price_factor(symbol=instrument_id)
                qf = get_qty_factor(symbol=instrument_id)
                data = pd.DataFrame(tfile.get_node(f"/{instrument_id}/depth")[begin:end])
                adjust_data(data, ADJUST_TYPE, pf, qf)
                if self.instrument_tag:
                    instrument_id = f"{self.instrument_tag}.{instrument_id}"
                yield iterOrderBookSequence(instrument_id, data)
            
            for instrument_id, begin, end in trades:
                data =  pd.DataFrame(tfile.get_node(f"/{instrument_id}/trade")[begin:end])
                pf = get_price_factor(symbol=instrument_id)
                qf = get_qty_factor(symbol=instrument_id)
                adjust_data(data, ADJUST_TYPE, pf, qf)
                if self.instrument_tag:
                    instrument_id = f"{self.instrument_tag}.{instrument_id}"
                yield iterTradeSequence(instrument_id, data)

   
class QuotationDataSource(object):

    def __init__(self, data_file_head: str, root: str = ".", cache_dir: str = "."):
        assert os.path.exists(root), f"Path not exists: {root}"
        self.data_root: str = root
        if not os.path.exists(cache_dir):
            os.makedirs(cache_dir)
        self.cache_dir: str = cache_dir
        self.data_file_head: str = data_file_head
        self.snapshot_head = f"merged_{self.data_file_head}"
        self._orderbooks = {}

    def get_quotation_table(self, timestamp: int):
        filename = self.filename_format(timestamp)
        return HDFQuotation(
            os.path.join(self.data_root, filename),
            self.data_file_head
        )
    
    def filename_format(self, timestamp):
        date = datetime.fromtimestamp(timestamp / 1000, tz=UTC)
        return f"{self.data_file_head}_quotation_{date.strftime('%Y%m%d')}.h5"

    def snapshot_format(self, timestamp):
        date = datetime.fromtimestamp(timestamp / 1000, tz=UTC)
        return f"{self.snapshot_head}_{date.strftime('%Y%m%d')}.h5"

    @staticmethod
    def save_orderbook_cache(cache_file: str, orderbook: dict):
        data = orderbook.copy()
        data["asks"] = list(data["asks"].items())
        data["bids"] = list(data["bids"].items())
        with open(cache_file, "w") as f:
            json.dump(data, f, use_decimal=True)

    @staticmethod
    def load_orderbook_cache(cache_file: str):
        with open(cache_file, "r") as f:
            orderbook = json.load(f, use_decimal=True)
            orderbook["asks"] = SortedDict(orderbook["asks"])
            orderbook["bids"] = SortedDict(neg, orderbook["bids"])
            return orderbook

    def orderbook_cache_file(self, instrument_id: str, begin: int):
        return os.path.join(self.cache_dir, f"{self.data_file_head}_cache_{instrument_id}_{begin}.json")
    
    def init_partial(self, instrument_ids: list, begin: int, use_cached: bool = True, pretreatment: bool = False) -> dict:
        orderbooks = self._orderbooks
        filename = os.path.join(self.data_root, self.filename_format(begin))
        if not os.path.exists(filename):
            raise FileNotFoundError(filename)
        if pretreatment:
            log_origin_partial_depth(filename, instrument_ids)
        tfile = tables.File(filename, "r")
        rgroup = tfile.get_node("/")
        for instrument_id in instrument_ids:
            if instrument_id not in rgroup:
                tfile.close()
                raise ValueError(f"Data of {instrument_id} not found in {filename}")
            cache_file = self.orderbook_cache_file(instrument_id, begin)
            if os.path.isfile(cache_file):
                # print(f"use cached orderbook: {cache_file}")
                orderbooks[instrument_id] = self.load_orderbook_cache(cache_file)
                # self.init_last_from_cache(instrument_id)
                continue
            group = rgroup[instrument_id]
            # ensure_ptimearray(tfile, group)
            ptime = get_partial_time(group, begin)
            if ptime < begin:
                if use_cached:
                    if instrument_id in orderbooks \
                            and ("localtime" in orderbooks[instrument_id]) \
                            and (orderbooks[instrument_id]["localtime"] < ptime):
                        ptime = orderbooks[instrument_id]["localtime"]

                    else:
                        orderbooks[instrument_id] = {
                            "asks": SortedDict(),
                            "bids": SortedDict(neg)
                        }
                else:
                    orderbooks[instrument_id] = {
                        "asks": SortedDict(),
                        "bids": SortedDict(neg)
                    }

                sequence = slicedSequence(instrument_id, group["depth"], int(ptime), int(begin))
                for ltime, mtype, data in sequence:
                    if mtype == 1:
                        orderbooks[instrument_id] = generatePartial(data)
                    else:
                        updatePartial(orderbooks[instrument_id], data)
                self.save_orderbook_cache(cache_file, orderbooks[instrument_id])
                print(f"save orderbook cache: {cache_file}")

        tfile.close()
        return orderbooks

    def make_starting_points(self, instrument_id: str, timestamp_list: list):
        diff = 24 * 3600000
        timestampes = {}
        for ts in timestamp_list:
            timestampes.setdefault(ts - ts % diff, set()).add(ts)
        for date_begin, time_set in timestampes.items():
            self._orderbooks = {}
            for ts in sorted(time_set):
                print("Make cache orderbook for:", datetime.fromtimestamp(ts / 1000, tz=UTC).isoformat())
                self.init_partial([instrument_id], ts, True)
    
    def init_starting_points(self, instrument_id: str, date: str, step: int = 3600000):
        date_begin = datetime.strptime(date, "%Y-%m-%d").replace(tzinfo=UTC).timestamp() * 1e3
        date_end = date_begin - date_begin % 24 * 3600000 + 24 * 3600000
        timestamp_list = list(range(int(date_begin), int(date_end), int(step)))
        self.make_starting_points(instrument_id, timestamp_list)

    def data_pretreatment(self, date: str, instrument_ids: list):
        date_begin = datetime.strptime(date, "%Y-%m-%d").replace(tzinfo=UTC).timestamp() * 1e3
        filename = os.path.join(self.data_root, self.filename_format(date_begin))
        if os.path.exists(filename):
            log_origin_partial_depth(filename, instrument_ids)
            return True
        return False


def make_time_points(begin: int, end: int, step: int=3600000):
    points = list(range(begin, end, step))
    if points[-1] < end:
        points.append(end)
    return points


class Backtest(object):
    

    MICROSECONDS_PER_DAY = int(24*3600*1000)

    def __init__(self):
        self.quote_sources: Dict[str, QuotationDataSource] = {}
        self.engine: TransactionEngine = TransactionEngine()
        self.step = 3600000 # 分块读取数据的时间间隔，microseconds
        self.data_root = "." # 本地数据文件所在目录
        self.cache_dir = "." # partial缓存所在目录
        self.use_cached = True # 是否启用partial缓存
        self.enable_data_pretreatment = True
    
    @classmethod
    def with_exchange(cls, *exchanges: str, root: str=".", cache_dir: str="."):
        """初始化时指定交易所数据源

        :param *exchanges: 交易所名称
        :type *exchanges: str
        :param root: 数据文件所在目录, defaults to "."
        :type root: str, optional
        :param cache_dir: partial缓存所在目录, defaults to "."
        :type cache_dir: str, optional
        :return: [description]
        :rtype: [type]
        """
        bt = cls()
        bt.data_root = root
        bt.cache_dir = cache_dir
        for exchange in exchanges:
            bt.add_quotation_source(exchange, root, cache_dir)
        return bt

    def add_quotation_source(self, exchange: str, root: str="", cache_dir: str=""):
        """添加交易所数据源

        :param exchange: 交易所名称，对应的数据文件名为：{exchange}_quotation_YYYYMMDD.h5
        :type exchange: str
        :param root: 数据文件所在目录, defaults to ""
        :type root: str, optional
        :param cache_dir: partial缓存所在目录, defaults to ""
        :type cache_dir: str, optional
        """
        if not root:
            root = self.data_root
        if not cache_dir:
            cache_dir = self.cache_dir
        
        self.quote_sources[exchange] = QuotationDataSource(exchange, root, cache_dir)
    
    def init_strategy(self, strategy_class: Type[HFTemplate], params: dict = None):
        """初始化策略

        :param strategy_class: 策略类
        :type strategy_class: HFTemplate
        :param params: 策略参数, defaults to None
        :type params: dict, optional
        """
        self.engine.init_strategy(strategy_class, params)

    def classify_instruments(self, instrument_ids: List[str]) -> Dict[str, List]:
        exchanges = list(self.quote_sources.keys())
        iids: Dict[str, List] = {key: [] for key in exchanges}
        for instrument_id in instrument_ids:
            exchange, iid = instrument_id.split(".", 1)
            iids[exchange].append(iid)
        for key in exchanges:
            if not iids[key]:
                del iids[key]
        return iids

    def run(self, instrument_ids: List[str], begin: int, end: int, customized_sources: Optional[List[DataSource]] = None):
        """运行本地回测

        :param instrument_ids: 回测品种，需要带交易所前缀：binance.eth_usdt_swap
        :type instrument_ids: List[str]
        :param begin: 开始时间，microseconds
        :type begin: int
        :param end: 结束时间，microseconds
        :type end: int
        :param customized_sources: 自定义数据源, defaults to None
        :type customized_sources: Optional[List[DataSource]], optional
        :return: [description]
        :rtype: [type]
        """

        if not customized_sources:
            customized_sources = []

        iids: Dict[str, List] = self.classify_instruments(instrument_ids)
        
        orderbooks = {}
        for exchange, ids in iids.items():
            qs: QuotationDataSource = self.quote_sources[exchange]
            _obs = qs.init_partial(ids, begin, self.use_cached, self.enable_data_pretreatment)
            for key, value in _obs.items():
                orderbooks[f"{exchange}.{key}"] = value
            
        self.engine.init_data(instrument_ids, orderbooks)

        while begin < end:
            day_tag = int(begin - begin % self.MICROSECONDS_PER_DAY)
            next_day = day_tag + self.MICROSECONDS_PER_DAY
            points: List = make_time_points(begin, min(next_day, end), self.step)
            indexes: Dict[str, Dict] = {}
            
            for exchange, ids in iids.items():
                qs: QuotationDataSource = self.quote_sources[exchange]
                qt: HDFQuotation = qs.get_quotation_table(begin)
                indexes[exchange] = qt.get_index(ids, points)
            
            for i in range(len(points) - 1):
                sequence = []
                for exchange, ids in iids.items():
                    qs: QuotationDataSource = self.quote_sources[exchange]
                    qt: HDFQuotation = qs.get_quotation_table(begin)
                    result = indexes[exchange]
                    depth_index = [(iid, result[iid]["depth"][i], result[iid]["depth"][i+1]) for iid in result.keys()]
                    trade_index = [(iid, result[iid]["trade"][i], result[iid]["trade"][i+1]) for iid in result.keys()]
                    _s = qt.batch_read(
                        depth_index,
                        trade_index,
                        self.engine.time_tag
                    )
                    sequence.extend(list(_s))
                
                for ds in customized_sources:
                    sequence.append(
                        map(lambda doc: (doc["timestamp"], 4, doc), ds.iter_data(begin, end))
                    )

                data_stream = sorted(chain(*sequence), key=lambda tp: tp[0:2])
                self.engine.run_loop(data_stream)

            begin = next_day
        
        self.engine.close_on_stop(end)
        return self.engine.strategy.onStop()

    def data_pretreatment(self, date: str, instrument_ids: list):
        iids: Dict[str, List] = self.classify_instruments(instrument_ids)
        for exchange, ids in iids.items():
            qs: QuotationDataSource = self.quote_sources[exchange]
            qs.data_pretreatment(date, ids)
            
