try:
    from hf_trade.backtest import Backtest, iterOrderBookSequence, adjustPrice, adjustQty, updatePartial, generatePartial, makeSortedDepth
except ImportError:
    import sys
    import os
    root, _ = os.path.split(os.path.abspath(__file__))
    sys.path.append(root)
    from backtest import Backtest, iterOrderBookSequence, adjustPrice, adjustQty, updatePartial, generatePartial, makeSortedDepth
import pandas as pd
from datetime import datetime, timezone, timedelta
import tables
import os
import numpy as np
import simplejson as json
from sortedcontainers import SortedList, SortedDict
from operator import neg
from decimal import Decimal

from bokeh.io import show, curdoc
from bokeh.models import ColumnDataSource, DataTable, DateFormatter, TableColumn, Selection, PreText, Slider
from bokeh.plotting import figure
from bokeh.layouts import column, row
import numpy as np

import argparse


UTC = timezone(timedelta())


def iter_sliced_orderbook(backtest: Backtest, instrument_id: str, begin: int, end: int, group_size=3600000):
    for day_begin, day_end in iter_range(begin, end, 24*3600000):
        filename = os.path.join(backtest.data_root, backtest.filename_format(day_begin))
        if os.path.exists(filename):
            tfile = tables.File(filename, "r")
            dname = f"/{instrument_id}/depth"
            dnode = tfile.get_node(dname)
            dtime = dnode.col("localtime")
            for b, e in iter_range(day_begin, day_end, group_size):
                bpos, epos = dtime.searchsorted(b), dtime.searchsorted(e)
                if bpos < epos:
                    depths = pd.DataFrame(dnode[bpos:epos])
                    depths["price"] = depths["price"].apply(adjustPrice)
                    depths["qty"] = depths["qty"].apply(adjustQty)
                    yield iterOrderBookSequence(instrument_id, depths)
            tfile.close()

def read_trades(backtest: Backtest, instrument_id: str, begin: int, end: int, group_size=3600000):
    all_trades = []
    for day_begin, day_end in iter_range(begin, end, 24*3600000):
        filename = os.path.join(backtest.data_root, backtest.filename_format(day_begin))
        if os.path.exists(filename):
            tfile = tables.File(filename, "r")
            tname = f"/{instrument_id}/trade"
            tnode = tfile.get_node(tname)
            ttime = tnode.col("localtime")
            bpos = ttime.searchsorted(day_begin)
            epos = ttime.searchsorted(day_end)
            trades = pd.DataFrame(tnode[bpos:epos])
            trades["price"] = trades["price"].apply(adjustPrice)
            trades["qty"] = trades["qty"].apply(adjustQty)
            trades["side"] = trades["side"].apply(lambda s: "B" if s==1 else "S")
            all_trades.append(trades)
            tfile.close()
    
    if len(all_trades) == 1:
        return all_trades[0]
    elif len(all_trades) > 1:
        return pd.concat(all_trades, ignore_index=True)
    else:
        return pd.DataFrame(columns=['id', "trade_id", "price", "qty", "side", "localtime", "timestamp", "datetime"])
                
def iter_range(begin: int, end: int, group_size=3600000):
    while begin < end:
        next_begin = begin - begin % group_size + group_size
        if next_begin < end:
            yield begin, next_begin
        else:
            yield begin, end
        begin = next_begin


def read_cache(filename: str):
    with open(filename, "r") as f:
        orderbook = json.loads(f.read(), use_decimal=True)
    
    asks = SortedDict()
    bids = SortedDict(neg)
    makeSortedDepth(asks, orderbook["asks"])
    makeSortedDepth(bids, orderbook["bids"])
    orderbook['asks'] = asks
    orderbook['bids'] = bids
    return orderbook


def save_cache(filename: str, orderbook: dict):
    dump_obj = orderbook.copy()
    dump_obj["asks"] = [list(item) for item in orderbook["asks"].items()]
    dump_obj["bids"] = [list(item) for item in orderbook["bids"].items()]
    with open(filename, "w") as f:
        json.dump(dump_obj, f, use_decimal=True)


def make_full_orderbook_table(backtest: Backtest, instrument_id: str, begin: int, end: int):
    cache_file = get_cache_name(instrument_id, begin)
    if os.path.exists(cache_file):
        orderbook = read_cache(cache_file)
    else:
        backtest.init_partial([instrument_id], begin)
        if instrument_id in backtest._orderbooks:
            orderbook = backtest._orderbooks[instrument_id]
        else:
            orderbook = generatePartial({"asks": [], "bids": []})
        save_cache(cache_file, orderbook)
    ticks = []

    for sequence in iter_sliced_orderbook(backtest, instrument_id, begin, end):
        for ltime, mtype, data in sequence:
            if mtype == 1:
                orderbook = generatePartial(data)
            else:
                updatePartial(orderbook, data)

            fd = make_full_depth(orderbook)
            ticks.append(fd)

    return ticks

def get_array(data, size):
    array = np.zeros((size, ), dtype=Decimal)
    array[:size] = data
    return array


def make_full_depth(orderbook: dict, size: int=200):
    return {
        "localtime": datetime.fromtimestamp(orderbook["localtime"]/1000, tz=UTC),
        "timestamp": datetime.fromtimestamp(orderbook["timestamp"]/1000, tz=UTC),
        "asks": {
            "price": np.array(orderbook["asks"].keys()[:size], dtype=Decimal),
            "volume": np.array(orderbook["asks"].values()[:size], dtype=Decimal),
        },
        "bids": {
            "price":  np.array(orderbook["bids"].keys()[:size], dtype=Decimal),
            "volume": np.array(orderbook["bids"].values()[:size], dtype=Decimal),
        }
    }

    
def get_cache_name(instrument_id: str, begin: int):
    return f"depth_{instrument_id}_{begin}.json"


class DepthDataContainer(object):

    def __init__(self, ticks: list, trades: pd.DataFrame, widget_config: dict=None, time_index: str= "datetime"):
        self.widget_config = {
            "plot_width": 1280,
            "depth_table_width": 240,
            "depth_table_height": 300,
        }
        if isinstance(widget_config, dict):
            self.widget_config.update(widget_config)
        self.ticks = ticks
        self.trades = trades
        trades["datetime"] = trades[time_index].apply(lambda t: datetime.fromtimestamp(t/1000, tz=UTC))
        self.trades_ts = trades[time_index].values 
        self.price_data = {
            "price": [(orderbook["asks"]["price"][0] + orderbook["bids"]["price"][0])/2 for orderbook in ticks],
            "datetime": [orderbook[time_index] for orderbook in ticks]
        }
        self.price_source = ColumnDataSource(self.price_data)

        self.ask_source = ColumnDataSource(
            self.ticks[0]["asks"]
        )
        self.bid_source = ColumnDataSource(
            self.ticks[0]["bids"]
        )
        self.trade_source = ColumnDataSource(
            self.trades.iloc[:1500]
        )


        self.selected_dot_source = ColumnDataSource(
            {
                "datetime": [self.price_data["datetime"][0]],
                "price": [self.price_data["price"][0]]
            }
        )

        self.main_plot = figure(tools='pan,box_zoom,wheel_zoom,reset', width=self.widget_config["plot_width"], x_axis_type="datetime")
        self.main_plot.line(x="datetime", y="price", source=self.price_source)
        self.main_plot.circle(x="datetime", y="price", source=self.selected_dot_source, color="red", size=8)

        self.table_columns = [
            TableColumn(field="price", title="price"),
            TableColumn(field="volume", title="volume"),
        ]

        self.trade_columns = [
            TableColumn(field="datetime", title="time", formatter=DateFormatter(format="%H:%M:%S")),
            TableColumn(field="side", title="side"),
            TableColumn(field="price", title="price"),
            TableColumn(field="qty", title="qty"),
        ]


        self.ask_table = DataTable(source=self.ask_source, columns=self.table_columns, width=self.widget_config["depth_table_width"], height=self.widget_config["depth_table_height"], sortable=True, scroll_to_selection=True)
        self.bid_table = DataTable(source=self.bid_source, columns=self.table_columns, width=self.widget_config["depth_table_width"], height=self.widget_config["depth_table_height"], sortable=True, scroll_to_selection=True)
        self.trade_table = DataTable(source=self.trade_source, columns=self.trade_columns, height=600, width=360, sortable=True, scroll_to_selection=True)
        
        self.texts = PreText(text=self.make_text(self.ticks[0]), width=500)

        self.slider = Slider(start=0, end=len(self.ticks)-1, value=0, step=1, width=self.widget_config["plot_width"])
        self.slider.on_change("value", self.on_slider_change)
    
    @staticmethod
    def make_text(orderbook: dict):
        return 'datetime: %s\nlocaltime: %s' % (str(orderbook["timestamp"]), str(orderbook["localtime"]))

    def on_slider_change(self, attrname, old, new):
        orderbook = self.ticks[new]
        dt = self.price_data["datetime"][new]
        self.ask_source.update(data=orderbook["asks"])
        self.bid_source.update(data=orderbook["bids"])
        self.selected_dot_source.update(data={"datetime": [dt], "price":[self.price_data["price"][new]]})
        self.texts.text = self.make_text(orderbook)
        trade_pos = self.trades_ts.searchsorted(dt.timestamp()*1000)
        self.trade_source.data = self.trades.iloc[max(trade_pos-1500, 0):trade_pos]
    
    def show(self):
        curdoc().add_root(
            row(
                column(
                    self.main_plot,
                    self.texts,
                    self.slider
                ),
                column(
                    self.ask_table,
                    self.bid_table
                ),
                self.trade_table
            )
        )
        print("show plot")


def make_ts(iso_text: str):
    dt = datetime.fromisoformat(iso_text)
    if not dt.tzinfo:
        dt = dt.replace(tzinfo=UTC)
    return int(dt.timestamp() * 1000)


def make_starting_points(instrument_id: str, datetimes: list, path: str="."):
    diff = 24 * 3600000
    timestampes = {}
    for dt in datetimes:
        ts = make_ts(dt)
        timestampes.setdefault(ts - ts % diff, set()).add(ts)
    backtest = Backtest()
    backtest.data_root = path
    for date_begin, time_set in timestampes.items():
        backtest._orderbooks = {}
        for ts in sorted(time_set):
            print("Make cache orderbook for:", datetime.fromtimestamp(ts/1000, tz=UTC).isoformat())
            cache_file = get_cache_name(instrument_id, ts)
            if os.path.exists(cache_file):
                orderbook = read_cache(cache_file)
                if "localtime" in orderbook:
                    backtest._orderbooks[instrument_id] = orderbook
            else:
                backtest.init_partial([instrument_id], ts, True)
                if instrument_id in backtest._orderbooks:
                    orderbook = backtest._orderbooks[instrument_id]
                else:
                    orderbook = generatePartial({"asks": [], "bids": []})
                save_cache(cache_file, orderbook)


def server_show(instrument_id: str, begin: str, end: str, path: str=".", time_index: str="localtime"):
    assert time_index in {"localtime", "timestamp"}, f"Invalid index type: {time_index}, only support localtime & timestamp."
    backtest = Backtest()
    backtest.data_root = path

    begin = make_ts(begin)
    end = make_ts(end)

    trades = read_trades(backtest, instrument_id, begin, end)


    ticks = make_full_orderbook_table(backtest, instrument_id, begin, end)
    print("data count", len(ticks))

    ddc = DepthDataContainer(ticks, trades, time_index=time_index)

    ddc.show()


main_parser = argparse.ArgumentParser(
    prog="Tick Review",
    description="Command line tool for tick data review.",
)

subparsers = main_parser.add_subparsers(dest='command')

slice_parser = subparsers.add_parser("slice", prog="slice", description="Make starting points for full orderbook sequence.")
slice_parser.add_argument("datetimes", nargs="+", help="Starting points you want to make, iso format.")
slice_parser.add_argument("-i", "--instrument_id", help="Choose an instrument id.")
slice_parser.add_argument("-p", "--path", default=".", help="Tick data path.")

main_parser.add_argument(
    "-r", "--range", 
    nargs=2,
    dest="range",
    help="Begin time and end time to show, iso format.",
    metavar=("BEGIN", "END")
)
main_parser.add_argument("-i", "--instrument_id", help="Choose an instrument_id.")
main_parser.add_argument("-p", "--path", default=".", help="Tick data path.")
main_parser.add_argument("--index", default="localtime", help="Index column: localtime (default) or timestamp")

argvs = main_parser.parse_args()

print(argvs)

if argvs.command == "slice":
    make_starting_points(argvs.instrument_id, argvs.datetimes, argvs.path)
else:
    server_show(argvs.instrument_id, argvs.range[0], argvs.range[1], argvs.path, time_index=argvs.index)