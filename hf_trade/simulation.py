import aioredis
import tables
from hf_trade.engine import TransactionEngine, HFTemplate, EngineStreamType, UTC, StopStrategy
from hf_trade.hdf_storage import RotateTableLogger, TableLogger
import os
from typing import Dict, List, Callable, Awaitable, Optional, Any, Type, Union
from decimal import Decimal
from sortedcontainers import SortedList, SortedDict
import json
from operator import neg
import asyncio
import logging
from datetime import datetime
import traceback


class OrderDescription(tables.IsDescription):

    order_id = tables.StringCol(16, pos=0)
    instrument_id = tables.StringCol(32, pos=1)
    order_type = tables.Int8Col(pos=2)
    side = tables.Int8Col(pos=3)
    price = tables.Float32Col(pos=4)
    qty = tables.Float32Col(pos=5)
    filled_qty = tables.Float32Col(pos=6)
    status = tables.Int8Col(pos=7)
    create_time = tables.Int64Col(pos=8)
    update_time = tables.Int64Col(pos=9)
    tag = tables.StringCol(32, pos=10)



class TransactionDescription(tables.IsDescription):

    transaction_id = tables.StringCol(16, pos=0)
    order_id = tables.StringCol(16, pos=1)
    instrument_id = tables.StringCol(32, pos=2)
    order_type = tables.Int8Col(pos=3)
    side = tables.Int8Col(pos=4)
    price = tables.Float32Col(pos=5)
    qty = tables.Float32Col(pos=6)
    trade_type = tables.StringCol(16, pos=7)
    timestamp = tables.Int64Col(pos=8)


class OrderLogger(object):

    def __init__(self, filename: str):
        self.filename = filename
        self.file = tables.File(filename, "a")
        if "/order" not in self.file:
            self.order_table: tables.Table = self.file.create_table("/", "order", OrderDescription)
            self.file.flush()
        else:
            self.order_table: tables.Table = self.file.get_node("/order")
        if "/transaction" not in self.file:
            self.transaction_table: tables.Table = self.file.create_table("/", "transaction", TransactionDescription)
            self.file.flush()
        else:
            self.transaction_table: tables.Table = self.file.get_node("/transaction")
    
    def __enter__(self):
        return self
    
    def __exit__(self, *args):
        self.close()

    def flush(self):
        self.file.flush()

    def close(self):
        if self.file.isopen:
            self.file.close()

    def on_orders(self, orders: List[Dict]):
        docs = [(
            order["order_id"][:16],
            order["instrument_id"],
            order["order_type"].value,
            order["side"],
            float(order["price"]),
            float(order["qty"]),
            float(order["filled_qty"]),
            order["status"].value,
            order["create_time"],
            order["update_time"],
            order["tag"][:32]
         ) for order in orders]
        self.order_table.append(docs)

    def on_transactions(self, transactions: List[Dict]):
        docs = [(
            transaction["transaction_id"],
            transaction["order_id"],
            transaction["instrument_id"],
            transaction["order_type"].value,
            transaction["side"],
            float(transaction["price"]),
            float(transaction["qty"]),
            transaction["trade_type"],
            transaction["timestamp"],

        ) for transaction in transactions]
        self.transaction_table.append(docs)

    def switch(self):
        pass


class RotateOrderLogger(OrderLogger):

    def __init__(self, log_dir: str):
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)
        self.log_dir = log_dir
        self.date = datetime.now(tz=UTC).date()
        super().__init__(self.current_filename())

    def current_filename(self):
        dt = self.date.strftime("%Y%m%d")
        return os.path.join(self.log_dir, f"orders_{dt}.h5")
    
    def switch(self):
        date = datetime.now(tz=UTC).date()
        if date != self.date:
            self.close()
            self.date = date
            super().__init__(self.current_filename())


TRADE_SIDE_MAP = {
    "buy": 1,
    "sell": -1
}


def rds2trade(trade: Dict):
    return {
        "trade_id": trade["tid"],
        "price": Decimal(trade["p"]),
        "qty": Decimal(trade["q"]),
        "side": TRADE_SIDE_MAP[trade["s"]],
        "localtime": trade["LMS"],
        "timestamp": trade["MMS"],
        "instrument_id": f"{trade['E']}.{trade['S']}"
    }


def makeSortedDepth(sd: Dict, levels: List[List]):
    for price, volume in levels:
        sd[Decimal(price)] = Decimal(volume)


def rds2depth(depth: Dict):

    # asks = SortedDict()
    # bids = SortedDict(neg)
    # makeSortedDepth(asks, depth["a"])
    # makeSortedDepth(bids, depth["b"])
    return {
        "localtime": depth["LMS"],
        "timestamp": depth["MMS"],
        "instrument_id": f"{depth['E']}.{depth['S']}",
        "asks": [(Decimal(level[0]), Decimal(level[1])) for level in depth["a"]],
        "bids": [(Decimal(level[0]), Decimal(level[1])) for level in depth["b"]]
    }


class RedisDataSource(object):

    def __init__(self, url: str, write_url: str = "", loop: Optional[asyncio.BaseEventLoop]=None):
        """Redis数据源

        :param url: Redis数据库地址
        :type url: str
        :param write_url: Redis数据库写地址，用于读写分离的数据库, defaults to ""
        :type write_url: str, optional
        :param loop: 协程事件循环, defaults to None
        :type loop: Optional[asyncio.BaseEventLoop], optional
        """
        self.url = url
        self.write_url = write_url
        self.pool: Optional[aioredis.ConnectionsPool] = None 
        self.write_pool: Optional[aioredis.ConnectionsPool] = None 
        self.loop = loop if isinstance(loop, asyncio.BaseEventLoop) else asyncio.get_event_loop()
        self._running = False

    async def init_pool(self):
        self.pool = await aioredis.create_redis_pool(self.url)
        if self.write_url:
            self.write_pool = await aioredis.create_redis_pool(self.write_url)
        else:
            self.write_pool = self.pool
        self._running = True

    async def subscribe(self, channel_name: str, handler: Callable[[str, str], Any]) -> asyncio.Task:
        ch = (await self.pool.subscribe(channel_name))[0]
        return self.loop.create_task(self.listener(ch, handler))
    
    async def subscribes(self, targets: Dict[str,  Callable[[str, str], Any]]) -> Dict[str, asyncio.Task]:
        tasks = {}
        channels: List[aioredis.Channel] = await self.pool.subscribe(*targets.keys())
        for ch in channels:
            chname = ch.name.decode()
            handler = targets[chname]
            tasks[chname] = self.loop.create_task(self.listener(ch, handler))
        return tasks

    async def listener(self, channel: aioredis.Channel, handler: Callable[[str, str], Any]):
        chname = channel.name.decode()
        while self._running and await channel.wait_message():
            msg = await channel.get(encoding="utf-8")
            handler(chname, msg)

    async def set_kv(self, key: str, value: str):
        return await self.write_pool.set(key, value)


class RealTimeSimulation(object):

    def __init__(self, loop: asyncio.BaseEventLoop, data_source: RedisDataSource, order_logger: Union[OrderLogger, RotateOrderLogger]):
        self.loop = loop
        self.data_source = data_source
        self.engine = TransactionEngine()
        self.tasks: Dict[str, asyncio.Task] = {}
        self.order_logger = order_logger
        self.table_loggers: Dict[str, TableLogger] = {}
        self._running = False
        self._external_message_queue = asyncio.Queue()
        self._output_queue = asyncio.Queue()
    
    @classmethod
    def from_params(cls, rds_url: str, rds_write_url: str="", order_file: str="", order_log_dir: str="", loop: Optional[asyncio.BaseEventLoop]=None):
        """通过参数构造

        :param rds_url: redis地址
        :type rds_url: str
        :param rds_write_url: redis写地址, 在读写分离的情况需要该地址, defaults to ""
        :type rds_url: str
        :param order_file: 订单记录文件, order_file和order_log_dir需要至少填一个, defaults to ""
        :type order_file: str, optional
        :param order_log_dir: 订单记录文件夹，order_file和order_log_dir需要至少填一个, defaults to ""
        :type order_log_dir: str, optional
        :param loop: 事件循环, defaults to None
        :type loop: Optional[asyncio.BaseEventLoop], optional
        :raises ValueError: [description]
        :return: 构造对象
        :rtype: RealTimeSimulation
        """
        if order_log_dir:
            order_logger = RotateOrderLogger(order_log_dir)
        elif order_file:
            order_logger = OrderLogger(order_file)
        else:
            raise ValueError("At least one param is required: order_file or order_log_dir.")
        if not isinstance(loop, asyncio.BaseEventLoop):
            loop = asyncio.get_event_loop()
        return cls(
            loop,
            RedisDataSource(rds_url, rds_write_url, loop=loop),
            order_logger
        )

    @classmethod
    def from_config(cls, filename: str, strategy_class: Optional[Type[HFTemplate]] = None, run: bool=True):
        """从配置启动，实时模拟会直接开始运行。

        config sample:
        >>> {
                "rds_url": "redis://localhost", // 数据裤地址
                "rds_write_url": "redis://localhost", // redis写地址, 在读写分离的情况需要该地址。
                "order_log_dir": "./order_logs", // 订单存储目录，如果没有会使用data_root
                "symbols": ["huobi.eth_usdt_swap", "okex.eth_usdt_swap"], // 数据订阅品种。 
                "strategy": { // 策略配置
                    "class": "module_name.ClassName", // 策略类导入路径
                    "params": {} // 策略参数 
                },
                "data_root": "./data", // 数据输出目录
                "output": { // 自定义输出配置
                    "data": { // output一级分类(文件名)
                        "type": "hdf", // output类型，目前只有hdf
                        "log_dir": "./data", // output输出文件目录，如果没有会使用data_root
                        "tables":  { // 这里定义hdf文件中的数据表
                            "test": [ // 表名 和 列定义
                                ["timestamp", "Int64"], // [列名, 数据类型]
                                ["pending", "Int64"]
                            ],
                            "test2": [
                                ["timestamp", "Int64"],
                                ["value", "Float64"],
                                ["tag", "String32"]
                            ]
                        }
                    }
                }
            }

        当前output支持的数据类型有：Int16, Int32, Int64, Int128, Float32, Float64, Float128, String[SIZE]

        :param filename: 配置文件(.json)路径
        :type filename: str
        :param strategy_class: 策略类，没有会从配置文件中导入, defaults to None
        :type strategy_class: Optional[HFTemplate], optional
        :return: RealTimeSimulation对象
        :rtype: RealTimeSimulation
        """

        with open(filename, "r") as f:
            config = json.load(f)
        
        logging.info(f"[config] {config}")

        data_root = config.get("data_root", ".")

        rts = cls.from_params(
            rds_url=config["rds_url"],
            rds_write_url=config.get("rds_write_url", ""),
            order_log_dir=config.get("order_log_dir", data_root)
        )

        if "output" in config:
            for name, conf in config["output"].items():
                ctype = conf.get("type", "")
                if ctype == "hdf":
                    logging.info(f"Init log table: {conf}")
                    log_dir = conf.get("log_dir", data_root)
                    head = conf.get("head", name)
                    table_conf = conf["tables"]
                    rts.table_loggers[name] = RotateTableLogger(
                        log_dir, 
                        head,
                        table_conf
                    )

                else:
                    logging.warning(f"Output type not supported: {ctype}")

        engine_config = config.get("engine", {})
        for key, value in engine_config.items():
            setattr(rts.engine, key, value)

        import importlib

        s_config = config["strategy"]
        if not strategy_class:
            module_name, cls_name = s_config["class"].rsplit(".", 1)
            strategy_class = getattr(importlib.import_module(module_name), cls_name)
        params = s_config["params"]
        rts.init_strategy(strategy_class, params)
        symbols = config["symbols"]
        if run:
            rts.start(symbols)
        return rts
        
    def init_strategy(self, strategy_class: Type[HFTemplate], params: Dict = None):
        """[summary]

        :param strategy_class: 策略类
        :type strategy_class: Type[HFTemplate]
        :param params: 策略参数, defaults to None
        :type params: Dict, optional
        """
        self.engine.init_strategy(strategy_class, params)
        self.engine.strategy.sendMessage = self.send_message
        self.engine.strategy.output = self.output

    def start(self, symbols: List[str]):
        """开始运行实时模拟

        :param symbols: 品种列表
        :type symbols: List[str]
        """
        if not self._running:
            self.loop.run_until_complete(self.run(symbols))

    async def run(self, symbols: List[str]):
        self._running = True
        self.engine.init_data(symbols, {})
        await self.data_source.init_pool()
        subs = {}
        for symbol in symbols:
            subs[f"{symbol}.fd"] = self.on_full_depth
            subs[f"{symbol}.trades"] = self.on_trade
        tasks = await self.data_source.subscribes(subs)
        self.tasks.update(tasks)
        self.tasks["order_storer"] = self.loop.create_task(self.order_storer())
        self.tasks["sync_message"] = self.loop.create_task(self.sync_message())
        await self.monitor()
        self.order_logger.close()
        for _, tl in self.table_loggers.items():
            tl: TableLogger
            tl.close()
    
    async def monitor(self):
        while self._running:
            for key, task in list(self.tasks.items()):
                task: asyncio.Task
                if task.done():
                    logging.warning(f"[task {key}] is done")
                    err = task.exception()
                    if isinstance(err, Exception):
                        err.__class__
                        msgs = traceback.format_tb(err.__traceback__)
                        err_stk = "".join(msgs)
                        logging.error(f"[task {key}] [{err.__class__} {err}]\n{err_stk}")
                        if isinstance(err, StopStrategy):
                            self._running = False
                            break
                    await self.restart_task(key)
                    
            await asyncio.sleep(5)
        self.loop.stop()

    async def restart_task(self, key: str):
        if key == "order_storer":
            self.tasks["order_storer"] = self.loop.create_task(self.order_storer())
        elif key == "sync_message":
            self.tasks["sync_message"] = self.loop.create_task(self.sync_message())
        elif key.endswith("fd"):
            self.tasks[key] = await self.data_source.subscribe(
                key, self.on_full_depth
            )
        elif key.endswith("trades"):
            self.tasks[key] = await self.data_source.subscribe(
                key, self.on_trade
            )

    def on_full_depth(self, ch: str, message: str):
        orderbook = rds2depth(json.loads(message))
        self.engine.on_stream(orderbook[self.engine.time_tag], EngineStreamType.full.value, orderbook)

    def on_trade(self, ch: str, message: str):
        trade = rds2trade(json.loads(message))
        self.engine.on_stream(trade[self.engine.time_tag], EngineStreamType.trade.value, trade)

    @staticmethod
    def order_filter(order: Dict):
        return bool(order["trades"])

    def store_order(self):
        orders = []
        transactions = []
        for order in self.engine._finished_order:
            if order["trades"]:
                orders.append(order)
                transactions.extend(order["trades"].values())

        if transactions:
            logging.info(f"[finished orders] {len(orders)}")
            logging.info(f"[transactions] {len(transactions)}")
            self.order_logger.on_orders(orders)
            self.order_logger.on_transactions(transactions)
            self.order_logger.flush()
        else:
            logging.info(f"[No transactions filled]")
        self.engine._finished_order.clear()
        
    async def order_storer(self):
        while self._running:
            self.order_logger.switch()
            self.store_order()
            self.perform_output()
            await asyncio.sleep(5)
    
    def send_message(self, key: str, value: Union[Dict, List]):
        self._external_message_queue.put_nowait((key, value))
    
    def output(self, key: str, value: dict):
        self._output_queue.put_nowait((key, value))

    def perform_output(self):
        if not self._output_queue.empty():
            records = {}
            while self._output_queue.qsize():
                key, value = self._output_queue.get_nowait()
                filetag, tablename = key.split("/", 1)
                if filetag not in records:
                    records[filetag] = {tablename: [value]}
                else:
                    fr = records[filetag]
                    if tablename in fr:
                        fr[tablename].append(value)
                    else:
                        fr[tablename] = [value]
            
            for filetag, fr in records.items():
                tl: TableLogger = self.table_loggers[filetag]
                tl.switch()
                for tablename, docs in fr.items():
                    tl.on_data(tablename, docs)
                
                tl.flush()

    async def sync_message(self): 
        while self._running:
            key, value = await self._external_message_queue.get()
            logging.info(f"[send message] {key} {value}")
            await self.data_source.set_kv(key, value)
            
            