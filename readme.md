# 高频撮合引擎

引擎依据原始盘口和交易数据对回测订单做仿真撮合，以求最大限度模拟策略在实盘的表现。

## 撮合逻辑

回测引擎撮合的原则为下单和成交时考虑市场容量对策略订单的影响，但不考虑策略订单对市场状态的影响。

具体的下单和撮合规则，以买单为例：


* 挂单，挂单根据档口数据和成交数据会有不同的操作。
  * 撮合规则，挂单通过成交数据撮合，当成交价小于挂单价时，对应的挂单会以挂单价成交，成交的顺序为：高价优先成交，同价位排队靠前的优先成交。
  * 挂单排队，挂单作为特殊的档口存在，不与真实档口合并，通过排队的方式决定挂单成交顺序。每个价位的挂单形成一条排队序列，序列中每一张单都有一个序列位置，表示该挂单在此价位下的成交优先级，位置低的单优先成交。
  * 订单序列位置，在同一价位下同时存在的订单，根据其下单时的档口状态，和之后的档口状态变化，其位置也会不同，在该价位成交时，只有位置为0的挂单才能成交。订单序列位置的规则为：
    * 下单时，位置序列=max(当前该价位档口市场订单数(该价位不存在为0)，当前该价位本地挂单最后一单的结束位置(订单结束位置=订单开始位置+订单下单量))
    * 档口发生变化时，位置>当前档口数量的挂单同步向前移动至第一张订单到档口数量位置。
    * 成交时，成交价位所有订单向前位移并将满足成交条件的单撮合。
* 成交模式，根据下单时的盘口状态成交模式可分为`taker`和`maker`
  * `maker` 成交价为挂单价。下单价格在对手盘一档以下，订单会以挂单的形式直接挂出。
  * `taker` 成交价格为对手价。下单价格在对手盘一档或以上，订单会从一档开始往上成交，直到订单全部成交或低于订单价的盘口被该订单成交完，如果结束后订单还未完全成交，剩下的部分会以limit单形式挂出。make单的成交价为成交对手盘的成交价，如果吃掉了不止一档对手盘，则成交会分多次触发。
* 链路时延，仿真撮合可以模拟链路延时，在回测初始化时可以设置单向链路时延，即策略请求到交易所的延迟和交易所回报到策略的时延。

## 数据结构

### 策略模板

```python
class backtest.HFTemplate(object):

    # 策略启动时调用，用于初始化。
    def onInit(self):
        pass

    # 策略运行结束时调用
    def onStop(self):
        pass

    # 全量推送回调函数
    def onL2Partial(self, depth):
        pass

    # 增量推送回调函数
    def onL2Update(self, depth):
        pass

    # 市场成交推送回调函数
    def onL2Trade(self, trade):
        pass 

    # 获取当前动态订单簿
    def getOrderbook(self, instrument_id):
        return orderbook

    # 订单状态变化回调函数
    def onOrder(self, order):
        pass

    # 发单函数，返回状态为New，状态更新由回调函数实现
    def sendOrder(self, instrument_id, order_type, side, price, qty):
        return order

    # 撤单函数，撤单状态更新由回调函数实现
    def cancelOrder(self, order_id):
        pass
    
```

### 数据结构

#### depth

深度数据推送，Dict。

|key|type|description|
|:-:|:-:|:-:|
|localtime|int|接收到数据时的本地时间戳，单位ms|
|timestamp|int|接收到数据的交易所时间戳，单位ms|
|instrument_id|str|交易标的名称|
|asks|numpy.array[][2]|卖档数据更新，按价格增序，[[price1, qty1], [price2, qty2], ...]|
|bids|numpy.array[][2]|买当数据更新，按价格降序，[[price1, qty1], [price2, qty2], ...]|

#### trade

成交数据推送，Dict。

|key|type|description|
|:-:|:-:|:-:|
|trade_id|str|成交订单号|
|price|float|成交价|
|qty|float|成交量|
|side|int {1, -1}|成交方向。1：买， -1：卖|
|localtime|int|接收到数据时的本地时间戳，单位ms|
|timestamp|int|接收到数据的交易所时间戳，单位ms|
|instrument_id|str|交易标的名称|


#### orderbook

动态订单簿，Dict。

|key|type|description|
|:-:|:-:|:-:|
|localtime|int|接收到数据时的本地时间戳，单位ms|
|timestamp|int|接收到数据的交易所时间戳，单位ms|
|instrument_id|str|交易标的名称|
|asks|sortedcontainers.SortedDict {price: qty}|全量卖档数据，按价格增序。|
|bids|sortedcontainers.SortedDict {price: qty}|全量买档数据，按价格降序。|

全量档口数据结构 `sortedcontainers.SortedDict` 查找方式可以分为2种：

* `按值查找` 与dict的查找方式一致
* `按序查找` 按排序方式通过索引值查找
  * sortedcontainers.SortedDict.peekitem(index=-1) => (key, value)
  * asks为增序排列，即 orderbook["asks"].peekitem(0) 返回卖一价格和单量。
  * bids为降序排列，即 orderbook["bids"].peekitem(0) 返回买一价格和单量。

更多参见文档：http://www.grantjenks.com/docs/sortedcontainers/sorteddict.html#sortedcontainers.SortedDict


#### order

策略创建的订单，交易所返回的正常订单状态更新，dict。

|key|type|description|
|:-:|:-:|:-:|
|order_id|str|策略生成的订单号|
|instrument_id|str|交易标的名称|
|side|int {1, -1}|成交方向。1：买， -1：卖|
|order_type|Enum OrderType|下单类型(开平)|
|price|float|订单价格|
|qty|float|下单量|
|price_avg|float|平均成交价格|
|filled_qty|float|以成交量|
|create_time|int|订单创建时间，单位ms|
|update_time|int|订单最近更新时间，单位ms|
|status|Enum OrderStatus|订单状态|
|last_fill_id|str|最新成交的transaction_id|
|trades|dict {transaction_id: transaction}|记录该订单的成交|


交易所返回的异常状态更新，dict。
status=OrderStatus.Error, 该状态目前只在撤单失败的时候发生(在单向时延>0的情况下，发生的撤单请求到达时订单已成交，此时交易所无法找到订单因此返回error)。

|key|type|description|
|:-:|:-:|:-:|
|order_id|str|订单号|
|status|Enum OrderStatus|订单状态|


#### transaction

自有订单的撮合成交，dict

|key|type|description|
|:-:|:-:|:-:|
|transaction_id|str|本次成交编号|
|order_id|str|本次成交对应订单的订单号|
|price|float|成交价|
|qty|float|成交量|
|trade_type|str|成交类型: "taker", "maker"|
|timestamp|int|成交时间，单位ms|


#### 枚举类型

```python
# 订单类型
class OrderType(Enum):
    
    # 开仓
    Open = 1
    # 平仓
    Close = 2

# 订单状态
class OrderStatus(Enum):
    # 错误
    Error = -1
    # 新建，策略发单到第一次收到交易所回报
    New = 1
    # 已挂单，收到交易所回报到全部成交或撤单
    Pending = 2
    # 全部成交
    Filled = 3
    # 已撤销
    Canceled = 4

```

### 回测引擎(撮合成交)

```python
class backtest.Backtest(object):

    def __init__(self):
        self.data_root = "."
        self.delay_ms = 0

    def init_strategy(self, strategy_class, params: dict=None):
        pass

    def run(self, instrument_ids: list, begin: int, end: int):
        pass

    def make_trade_log(self):
        return pandas.DataFrame(orders), pandas.DataFrame(transactions)


```

#### 回测属性

|name|type|description|
|:-:|:-:|:-:|
|data_root|str|数据文件目录|
|delay_ms|int|单向时延ms|


```python
Backtest.init_strategy(strategy_class, params: dict=None):
```

初始化策略，传入策略类和参数。

|name|type|description|
|:-:|:-:|:-:|
|strategy_class|type|策略类，继承`HFTemplate`|
|params|dict|传入参数，作为属性赋值给策略|


```python
Backtest.run(instrument_ids: list, begin: int, end: int, group_size=3600000):
```

运行

|name|type|description|
|:-:|:-:|:-:|
|instrument_ids|list|品种列表|
|begin|int|时间戳毫秒|
|end|int|时间戳毫秒|
|group_size|int|时间长度毫秒，每次读取数据分组时间长度，防止占用过多内存|


```python
Backtest.make_trade_log() => DataFrame(orders), DataFrame(trades)
```

返回order和trade表


## sample


```python
import pandas as pd
import numpy as np
from datetime import datetime, timedelta, timezone
import backtest
from backtest import FINISHED_STATUS, OrderType, OrderStatus



class TestStrategy(backtest.HFTemplate):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.tcount = {}
        self.openOrder = {}
        self.closeOrder = {}
        self.orders = {}

    # 订单状态更新回调函数
    def onOrder(self, order):
        print("On order", order)
        oid = order["order_id"]
        # 订单不存在不进行后续处理
        if oid not in self.orders:
            print(f"order:{oid} not found.")
            return 
        
        # 该订单已完成不进行后续处理
        if self.orders[oid]["status"] in FINISHED_STATUS:
            print(f"order:{oid} already finished: {self.orders[oid]['status']}.")
            return 
        
        # 更新订单状态
        self.orders[order["order_id"]] = order
        iid = order["instrument_id"]

        # 平仓
        if order["order_type"] == OrderType.Close:
            self.closeOrder[iid] = order
            # 平仓单已完成则删除该订单和对应开仓
            if order["status"] in FINISHED_STATUS:
                oo = self.openOrder.pop(iid, None)
                co = self.closeOrder.pop(iid, None)
                if oo:
                    self.orders.pop(oo["order_id"], None)
                if co:
                    self.orders.pop(co["order_id"], None)
        # 开仓
        elif order["order_type"] == OrderType.Open:
            self.openOrder[iid] = order
            # 开仓被撤单则删除开仓
            if order["status"] == OrderStatus.Canceled:
                self.openOrder.pop(iid, None)
                self.orders.pop(order["order_id"], None)

    # 行情成交回调函数
    # 每个品种每100张成交下单一次，如果没有开仓则开仓，有开仓平仓，如果上一次下单未成交则撤单。
    def onL2Trade(self, trade):
        iid = trade["instrument_id"]
        self.tcount[iid] += 1
        if self.tcount[iid] % 100 == 0:
            depth = self.getOrderbook(iid)
            if iid not in self.openOrder:
                price, qty = depth["bids"].peekitem(1)
                order = self.sendOrder(
                    iid,
                    OrderType.Open,
                    1, 
                    price,
                    1
                )
                self.openOrder[iid] = order
                self.orders[order['order_id']] = order
                return 
            else:
                if self.openOrder[iid]["status"] != OrderStatus.Filled:
                    self.cancelOrder(self.openOrder[iid]["order_id"])
                    return
            if iid not in self.closeOrder:
                if self.openOrder[iid]["status"] == OrderStatus.Filled:
                    price, qty = depth["bids"].peekitem(1)
                    order = self.sendOrder(
                        iid,
                        OrderType.Close,
                        -1,
                        price,
                        1   
                    )
                    self.closeOrder[iid] = order
                    self.orders[order['order_id']] = order
            

def test_engine():
    # 品种，时间，backtest.UTC为预先定义号的utc时区类型。
    instruments = ["btc_usd_cq", "eos_usd_cq"]
    begin = datetime(2020, 3, 26, tzinfo=backtest.UTC).timestamp()
    end = datetime(2020, 3, 26, 12, tzinfo=backtest.UTC).timestamp()

    start = datetime.now()

    # 初始化回测
    bt = backtest.Backtest()
    # 设置单向时延
    bt.delay_ms = 100
    # 设置数据路径为当前路径
    bt.data_root = "."

    # 回测范围只包含20200326当天，因此当前目录下需要有文件okex_quotation_20200326.h5

    # 初始化策略类和参数
    bt.init_strategy(TestStrategy, {"tcount": dict.fromkeys(instruments, 0)})

    # 运行策略，起止时间单位为毫秒
    bt.run(
        instruments,
        int(begin*1000),
        int(end*1000)
    )
    print("run backtest", datetime.now()-start)

    # 生成订单和成交表输出到csv
    orders, transactions = bt.make_trade_log()
    orders.to_csv("orders_delay.csv")
    transactions.to_csv("transactions_delay.csv")


def main():
    test_engine()


if __name__ == "__main__":
    main()

```

