from hf_trade.simulation import RealTimeSimulation
import sys
import logging


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO, 
        format="%(asctime)s | %(levelname)s | %(module)s:%(lineno)d | %(message)s",
    )

    argvs = sys.argv[1:]
    if len(argvs) > 0:
        conf_file = argvs[0]
    else:
        conf_file = "trader_config.json"
    RealTimeSimulation.from_config(conf_file)